package sium.app;

import inpro.incremental.PushBuffer;
import inpro.incremental.sink.FrameAwarePushBuffer;
import inpro.incremental.source.SphinxASR;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import sium.module.ResolutionModuleRMRS;
import sium.nlu.context.Context;
import sium.nlu.grounding.Grounder;
import sium.nlu.language.LingEvidence;
import sium.system.util.PentoSqlUtils;
import sium.system.util.TakeSqlUtils;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4ComponentList;

public class TakeRMRSDemo {
	
	static Logger log = Logger.getLogger(PentoRMRSDemo.class.getName());
	
	@S4Component(type = SphinxASR.class)
	public final static String PROP_ASR_HYP = "currentASRHypothesis";

	@S4ComponentList(type = PushBuffer.class)
	public final static String PROP_HYP_CHANGE_LISTENERS = SphinxASR.PROP_HYP_CHANGE_LISTENERS;
	

	int currentFrame = 0;
	List<PushBuffer> hypListeners = null;

	List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();
	private TakeSqlUtils take;

	private int max = 1000;
	private int numFolds = 10;
	private int foldSize = 100;
	
	public TakeRMRSDemo() {
		
	}
	

	private void run() throws SQLException, InterruptedException {
		
		double correct = 0.0;
		double total = 0.0;

		for (int i=1; i<=3; i++) {
			int j = 1;
			
			take = new TakeSqlUtils();
			take.createConnection(null);
			ArrayList<String> episodes = take.getAllEpisodes();
			ConfigurationManager cm = new ConfigurationManager("src/sium/config/config.xml");
			PropertySheet ps = cm.getPropertySheet(PROP_ASR_HYP);
			hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
			ResolutionModuleRMRS resolution =  (ResolutionModuleRMRS) cm.lookup("sium_rmrs");
			
			System.out.println("Processing fold " + i + " out of " + numFolds);
			resolution.toggleTrainMode();
			
//			setup the training data
			for (String episode : episodes) {
				if (exceedsMax(j)) break;
			
				if (isTrainData(j, i)) {
//					System.out.println(episode);
					
					setContext(resolution, episode);
					
					ArrayList<LingEvidence> ling = take.getLingEvidence(episode);
					setGoldSlots(resolution, episode);
					
					processLine(ling);
				}
				j++;
			}
			
			resolution.train();
			resolution.toggleNormalMode();
			
			
//			now, run evaluation on this fold
			j = 0;
			for (String episode : episodes) {
				if (exceedsMax(j)) break;
			
				if (!isTrainData(j, i)) {
					
					setContext(resolution, episode);
					
					ArrayList<LingEvidence> ling = take.getLingEvidence(episode);
					setGoldSlots(resolution, episode);
					processLine(ling);
					
//					Now, evaluate....
//					This, of course, isn't right, it goes through each slot with an x entity, looking for the gold
					String gold = take.getGoldPiece(episode);
					HashMap<String, Grounder<String, String>> grounders = resolution.getGrounders();
					for (String gid : grounders.keySet()) {
						Grounder<String, String> grounder = grounders.get(gid);
						if (!gid.contains("x")) continue;
						if (!grounder.getPosterior().isEmpty()) {
							
							String guess = grounder.getPosterior().getArgMax().getEntity();
//							String guess = grounder.getPosterior().getItem(1).getEntity();
//							System.out.println(gold + " " + guess);
							if (gold.equals(guess)) {
								correct++;
								break;
							}
						}
					}
					total++;
				}
				j++;
			}
			
			resolution.clear();
			take.closeConnection();
			System.gc();
			Thread.sleep(1000);
			System.out.println("Accuracy: " + (correct / total) + "(" +correct+ "/" + total +")");
		}
	}
	
	private void setGoldSlots(ResolutionModuleRMRS resolution, String episode) throws SQLException {
		String gold = take.getGoldPiece(episode);
		resolution.setReferent("x", gold);
	}

	private void setContext(ResolutionModuleRMRS resolution, String episode) throws SQLException {
		Context<String,String> context = take.getContext(episode);
		context.setContextID("x");
		resolution.setContext(context);
	}

	private void processLine(ArrayList<LingEvidence> ling) {
		
		ArrayList<WordIU> ius = new ArrayList<WordIU>();
		
		WordIU prev = WordIU.FIRST_WORD_IU;
		
		for (LingEvidence ev : ling) {
			String word = ev.getValue("w1");
			if (word.equals("<s>")) continue;
			WordIU wiu = new WordIU(word, prev, null);
			ius.add(wiu);
			edits.add(new EditMessage<IU>(EditType.ADD, wiu));
			notifyListeners(hypListeners);
			prev = wiu;
		}
		
		for (WordIU iu : ius) {
			edits.add(new EditMessage<IU>(EditType.COMMIT, iu));
		}
		notifyListeners(hypListeners);
		
	}

	private boolean isTrainData(int j, int i) {
		return (j < (i-1) * foldSize) || (j > i * foldSize);
	}


	private boolean exceedsMax(int j) {
		return j > max;
	}

	public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			//logger.debug("notifying about" + edits);
			currentFrame += 100;
			for (PushBuffer listener : listeners) {
				if (listener instanceof FrameAwarePushBuffer) {
					((FrameAwarePushBuffer) listener).setCurrentFrame(currentFrame);
				}
				// notify
				listener.hypChange(null, edits);
				
			}
			edits.clear();
		}
	}		
	
	public static void main(String[] args) {
		
		try {
			new TakeRMRSDemo().run();
		}
		catch (SQLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
