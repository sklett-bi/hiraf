package sium.app;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JTextField;

import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4ComponentList;
import inpro.apps.SimpleReco;
import inpro.apps.SimpleText;
import inpro.apps.util.RecoCommandLineParser;
import inpro.incremental.IUModule;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.TextBasedFloorTracker;
import inpro.incremental.sink.FrameAwarePushBuffer;
import inpro.incremental.sink.LabelWriter;
import inpro.incremental.source.IUDocument;
import inpro.incremental.source.SphinxASR;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;

public class Interactive {
	
//	@S4Component(type = WebSpeech.class)
//	public final static String PROP_CURRENT_HYPOTHESIS = "webSpeech";
	
	@S4Component(type = SphinxASR.class)
	public final static String PROP_CURRENT_HYPOTHESIS = "currentASRHypothesis";	
	
	@S4Component(type = TextBasedFloorTracker.Listener.class)
	public final static String PROP_FLOOR_MANAGER_LISTENERS = TextBasedFloorTracker.PROP_STATE_LISTENERS;
	
	@S4Component(type = TextBasedFloorTracker.class)
	public final static String PROP_FLOOR_MANAGER = "textBasedFloorTracker";	
	
	private static ConfigurationManager cm;
	private static IUDocument iuDocument;
	private static PropertySheet ps;
	
	static List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();
	static int currentFrame = 0;
	static JTextField textField;
	
	public static double decisionPoint = 0.25;
	
	public static void main(String[] args) {
		new Interactive().run();
	}

	private void run() {
		try {
			cm = new ConfigurationManager(new File("src/sium/config/config.xml").toURI().toURL());
			ps = cm.getPropertySheet(PROP_CURRENT_HYPOTHESIS);
//			WebSpeech webSpeech = (WebSpeech) cm.lookup(PROP_CURRENT_HYPOTHESIS);
//			final List<PushBuffer> hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
//			TextBasedFloorTracker textBasedFloorTracker = (TextBasedFloorTracker) cm.lookup(PROP_FLOOR_MANAGER);
			iuDocument = new IUDocument();
//			iuDocument.setListeners(hypListeners);
//			SimpleText.createAndShowGUI(hypListeners, textBasedFloorTracker);
				
//	    	SimpleReco simpleReco = new SimpleReco(cm);	
//	    	simpleReco.recognizeInfinitely();
//	    	simpleReco.getRecognizer().deallocate();

		} 
		catch (PropertyException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
//		catch (UnsupportedAudioFileException e) {
//			e.printStackTrace();
//		} 
	}
	
	static public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			//logger.debug("notifying about" + edits);
			currentFrame += 100;
			for (PushBuffer listener : listeners) {
				if (listener instanceof FrameAwarePushBuffer) {
					((FrameAwarePushBuffer) listener).setCurrentFrame(currentFrame);
				}
				// notify
				listener.hypChange(null, edits);
				
			}
			edits = new ArrayList<EditMessage<IU>>();
		}
	}

}
