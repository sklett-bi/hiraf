package sandbox;

import inpro.incremental.source.GoogleASR;
import ipaaca.LocalMessageIU;
import ipaaca.OutputBuffer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import esslli.InterruptManager;

public class BMLAction {

	static OutputBuffer outBuffer = new OutputBuffer("BML");

	public static int id = 0;
	
	public static void sendBML(String bml) {
			
		if(GoogleASR.asrDemo)
			return;
		
		LocalMessageIU iu = new LocalMessageIU();
		iu.setCategory("realizerRequest");
		iu.getPayload().put("type", "bml");
		iu.getPayload().put("request", bml);
		outBuffer.add(iu);
		
		if(!bml.contains("breath") && !bml.contains("gaze"))
			id++;

		if(bml.contains("raiseid")) {
			new Thread() {
				public void run() {
					try {
						Thread.sleep(500);
						String breathInterruptbml = "";
						try {
							breathInterruptbml = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/interrupt.xml")));
						} catch (IOException e) {
							e.printStackTrace();
						}
						breathInterruptbml = breathInterruptbml.replace("bml$REPLACE_ID", "raise0");	
						BMLAction.sendBML(breathInterruptbml);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}.start();
		}
		
	}

	public static String createBML(String utterance) {

		String bml = "<bml xmlns=\"http://www.bml-initiative.org/bml/bml-1.0\"  id=\"bml" + id
				+ "\" xmlns:bmlt=\"http://hmi.ewi.utwente.nl/bml\">" + "<speech id=\"s1\">"
				+ "<description priority=\"1\" type=\"application/ssml+xml\">"
				+ "<speak xmlns=\"http://www.w3.org/2001/10/synthesis\">";

		bml += "" + utterance + "</speak>" + "</description>" + "</speech>\n" + "</bml>";
				
		return bml;

	}

	public static void createAndSendBML(String utterance) {

		if (utterance.equals(""))
			return;

		InterruptManager.handleInterrupt();

		String bml = createBML(utterance);
		sendBML(bml);

	}

}
