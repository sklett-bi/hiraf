package sandbox;

import java.util.List;
import java.util.Random;

public class QuizUtils {

	List<QuizBowlEntry> quizEntries;
	QuizBowlEntry activeEntry;
	int chunkIndex = 0;

	public QuizUtils() {
		quizEntries = QuizBowlParser.createEntries();
	}

	public void startQuestion() {

		chunkIndex = 0;
		Random r = new Random();
		int index = r.nextInt(quizEntries.size());
		activeEntry = quizEntries.get(index);

		BMLAction.createAndSendBML(activeEntry.questionChunks.get(chunkIndex));

	}

	public void offerAnswer(String question) {

		String answer = quizEntries.get(Integer.parseInt(question.replace("question", "")) - 1).answer;
		try {
			BMLAction.createAndSendBML("is " + answer + " correct?");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void continueQuestion() {

		if (chunkIndex < activeEntry.questionChunks.size() - 1)
			chunkIndex++;

		try {
			BMLAction.createAndSendBML(activeEntry.questionChunks.get(chunkIndex));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void repeat() {

		System.out.println(activeEntry.questionChunks.get(chunkIndex));
		try {
			BMLAction.createAndSendBML(activeEntry.questionChunks.get(chunkIndex));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void checkAnswer(String answer) {

		String utterance = "";
		if (activeEntry.id == Integer.parseInt(answer.replace("answer", ""))) {
			utterance = "nice job!";
		} else {
			utterance = "no, try again";
		}
		try {
			BMLAction.createAndSendBML(utterance);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
