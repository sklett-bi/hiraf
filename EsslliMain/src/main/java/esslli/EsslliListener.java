package esslli;

import inpro.incremental.source.GoogleASR;
import ipaaca.AbstractIU;
import ipaaca.HandlerFunctor;
import ipaaca.IUEventHandler;
import ipaaca.IUEventType;
import ipaaca.Initializer;
import ipaaca.InputBuffer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.Random;
import java.util.Set;

import module.DialogueManager;
import module.NLGModule;
import sandbox.BMLAction;

import com.google.common.collect.ImmutableSet;

public class EsslliListener implements Runnable {

	public static NLGModule nlg;
	public static TurnTakingManager tt;
	public static Thread t;
	public static InterruptManager im;
	public static Thread interruptThread;
	public static InputBuffer inputBuffer;
	public static long startTime = 0;

	private long lastDecisionEvent = 0;
	private long lastASREvent = 0;

	private long systemEndTimeReal = 0;
	private long lastNoDecisionAct = 0;

	public static boolean gazeInProgress = false;
	private boolean lastGazeLeft = true;
	public static boolean returnedGaze = false;
	public static int gazeId = -1;
	public static boolean beforeFirstVad = true;
	private boolean extraBlock = false;

	@Override
	public void run() {

		// the following categories are important for some features of the system:
		// vad, maryttsreply, im, nlg, asrword, dm
		// For logging are also possible: "nvbg", "maryttsrequest", "realizerRequest"
		Set<String> categories = new ImmutableSet.Builder<String>().add("esslli", "vad", "asrresults", "nlu", "inlu",
				"nlg", "tree", "asrword", "dm", "tt", "im", "asrraw", "maryttsreply", "realizerRequestOFF", "commit",
				"feedback").build();
		
		Set<String> categoriesDemo = new ImmutableSet.Builder<String>().add("vad","asrresults", "asrraw").build();
		

		final class MyEventHandler implements HandlerFunctor {
			@Override
			public void handle(AbstractIU iu, IUEventType type, boolean local) {

				long worldTime = System.currentTimeMillis() - startTime;

				if (iu.getCategory().equals("asrword")) {
					lastASREvent = worldTime;
					// FLAG: event handling
					// this is how you could link a action to every new word detected.
					// parameter has to be a specified action in nlg.xml
					// nlg.executeAction("nod");
				}

				if (iu.getCategory().equals("inlu")) {
					// lastNLUEvent = worldTime;
				}
				if (iu.getCategory().equals("dm")) {
					lastDecisionEvent = worldTime;
					DialogueManager.setVariable("puzzlement", 0);
				}

				if (iu.getCategory().equals("nlg") && iu.getPayload().containsKey("executedAction")) {

					DialogueManager.setVariable("puzzlement", 0);
					extraBlock = true;
					new Thread() {
						public void run() {
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							extraBlock = false;
						}
					}.start();
				}

				if (iu.getCategory().equals("realizerRequest")) {
					if (iu.getPayload().get("request").contains("MORPH"))
						return;
				}

				if (iu.getCategory().equals("maryttsreply")) {
					if (iu.getPayload().get("marks").length() == 0)
						return;
				}

				if (startTime == 0) {
					worldTime = 0;
				}

				String cat = "";
				if (iu.getCategory().equals("feedback"))
					cat = "feedb.";
				else if (iu.getCategory().equals("maryttsreply"))
					cat = "tts";
				else
					cat = iu.getCategory();
						
				String toPrint = iu.getPayload().toString();
				if(GoogleASR.asrDemo && cat.equals("asrraw")) {
					
					toPrint = toPrint.substring(37);
				}

				
				System.out.println(worldTime + "\t" + cat + "\t: " + toPrint);

				if (startTime == 0 && iu.getCategory().equals("vad") && iu.getPayload().containsKey("calibration")
						&& iu.getPayload().get("calibration").equals("finished")) {
					startTime = System.currentTimeMillis();
					tt.lastUserSpeechEnd = startTime;

					Thread firstTurn = new Thread(tt);
					firstTurn.start();
				}

				if (cat.equals("asrword") && startTime == 0)
					startTime = System.currentTimeMillis();

				if (iu.getCategory().equals("maryttsreply")) {

					try {
						String marks = iu.getPayload().get("phonems");
						String[] splitted = marks.split("\\(");
						String lastTimeStamp = splitted[splitted.length - 1].replace(")]", "").replace(";", "");
						EsslliContext.systemSpeaking = true;
						long ttsTimeNeeded = (long) (Double.parseDouble(lastTimeStamp) * 1000l);

						Thread t = new Thread() {
							public void run() {
								try {

									EsslliContext.lastSystemSpeechTime = System.currentTimeMillis() + ttsTimeNeeded;
									Thread.sleep(ttsTimeNeeded);
									EsslliContext.systemSpeaking = false;
									systemEndTimeReal = System.currentTimeMillis();
									Thread.sleep(tt.silenceTolerance);
									long worldTimeNow = System.currentTimeMillis() - startTime;
									if (!EsslliContext.systemSpeaking
											&& worldTimeNow - lastASREvent > tt.silenceTolerance) {
										if (System.currentTimeMillis() - systemEndTimeReal > tt.silenceTolerance - 1000) {
											tt.lastUserSpeechEnd = Long.MAX_VALUE;
											if(DialogueManager.getVariable("userRole").equals("contestant")
													&& DialogueManager.getVariable("salientAnswer") != "none") {
												nlg.executeAction("offerNextTip");
											} else {
												nlg.executeAction("default");
											}
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						};
						t.start();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				if (iu.getCategory().equals("im")) {
					InterruptManager.handleInterrupt();
					return;
				}

				if (iu.getCategory().equals("vad") && iu.getPayload().containsKey("interruptRequest")) {
					InterruptManager.handleInterrupt();
					return;
				}

				if (iu.getCategory().equals("vad") && iu.getPayload().containsKey("userSpeaks")) {

					if (beforeFirstVad)
						beforeFirstVad = false;

					if (Double.parseDouble(iu.getPayload().get("userSpeaks")) > 0.5) {

						returnedGaze = true;
						gazeInProgress = true;
						String interrupt = "interrupt.xml";
						returnedGaze = true;
						gazeInProgress = true;
						String bbbml = "";
						try {
							bbbml = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/" + interrupt)));
						} catch (IOException e) {
							e.printStackTrace();
						}
						bbbml = bbbml.replace("bml$REPLACE_ID", "gaze0");

						BMLAction.sendBML(bbbml);
						new Thread() {
							public void run() {
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								gazeInProgress = false;
							}
						}.start();

						nlg.plannedAction = null;
						tt.lastUserSpeechEnd = Long.MAX_VALUE;
						tt.lastUserSpeechStart = System.currentTimeMillis();
					} else {
						tt.lastUserSpeechEnd = System.currentTimeMillis();

						if (t == null || !t.isAlive()) {
							t = new Thread(tt);
							t.start();
						}
					}
					return;
				}

			}
		}

		tt = new TurnTakingManager();
		tt.nlg = nlg;
		im = new InterruptManager(nlg);

		if (tt.interruptAllowed) {
			interruptThread = new Thread(im);
			interruptThread.start();
		}

		Initializer.initializeIpaacaRsb();
		if(!GoogleASR.asrDemo) 
			inputBuffer = new InputBuffer("log", categories);
		else
			inputBuffer = new InputBuffer("log", categoriesDemo);
		
		EnumSet<IUEventType> types = EnumSet.allOf(IUEventType.class);
		inputBuffer.registerHandler(new IUEventHandler(new MyEventHandler(), types, categories));

		// reactToTimeThread
		new Thread() {
			public void run() {

				while (true) {
					long worldTime = System.currentTimeMillis() - startTime;

					double sadNew = 0;
					if (worldTime - lastDecisionEvent > 2000 && !EsslliContext.systemSpeaking) {
						if (worldTime - lastASREvent < worldTime - lastDecisionEvent) {

							sadNew = 0 + ((worldTime - lastDecisionEvent - 2000d) / 7000d);
							if (sadNew > 0.8) {
								sadNew = 0.8;
							}
						}
					}
					if (startTime != 0) {
						double sadOld = Double.parseDouble(DialogueManager.getVariable("puzzlement"));

						if (sadNew - sadOld > 0.1) {
							sadNew = sadOld + 0.1;
						}
						if (sadNew - sadOld < -0.1) {
							sadNew = sadOld - 0.1;
						}
						if (sadNew > 0.6) {
							sadNew = 0.6;
						}
						if (sadNew < 0) {
							sadNew = 0;
						}

						DialogueManager.setVariable("puzzlement", sadNew);

						if (sadNew > 0.2 && sadOld < 0.2) {
							nlg.sendFeedbackBML("tilt");
						}
					}

					// no DM, long vad -> interrupt
					if (worldTime - lastDecisionEvent > 1000 && lastNoDecisionAct < lastASREvent) {

						// if there's no DM, no ASR, but there has been ASR x seconds ago
						boolean forced = false;
						int softNoDecision = 2000;

						if (tt.lastUserSpeechEnd != Long.MAX_VALUE
								&& System.currentTimeMillis() - tt.lastUserSpeechEnd > softNoDecision) {
							forced = true;
						}

						int delayFromSpeechStart = 1500;
						if (DialogueManager.getVariable("userRole").equals("master")) {
							delayFromSpeechStart = 9000;
						}

						if (forced
								|| (tt.lastUserSpeechEnd == Long.MAX_VALUE
										&& System.currentTimeMillis() - tt.lastUserSpeechStart > delayFromSpeechStart
										&& System.currentTimeMillis() - tt.lastUserSpeechStart < tt.silenceTolerance - 500 && worldTime
										- lastDecisionEvent > delayFromSpeechStart)) {
							if (!EsslliContext.systemSpeaking && System.currentTimeMillis() - systemEndTimeReal > 4000) {
								if (tt.lastUserSpeechStart > systemEndTimeReal && !extraBlock) {
									if (!forced) {
										nlg.sendFeedbackBML("raise");
									}
									nlg.plannedAction = "nodecision";
									im.interrupt();
									lastNoDecisionAct = System.currentTimeMillis() - startTime;
								}
							}
						}
					}

					if (!gazeInProgress
							&& ((System.currentTimeMillis() - systemEndTimeReal > 4000
									&& tt.lastUserSpeechEnd == Long.MAX_VALUE && tt.lastUserSpeechStart < systemEndTimeReal)
									|| (tt.lastUserSpeechEnd != Long.MAX_VALUE && System.currentTimeMillis()
											- tt.lastUserSpeechEnd > 2000) || (beforeFirstVad && worldTime > 2000 && startTime != 0))
							&& !EsslliContext.systemSpeaking) {
						String bb = "gaze.xml";

						returnedGaze = false;
						gazeInProgress = true;
						String bbbml = "";
						try {
							bbbml = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/" + bb)));
						} catch (IOException e) {
							e.printStackTrace();
						}
						int xCoord = 0;
						if (lastGazeLeft) {
							xCoord = 1;
							lastGazeLeft = false;
						} else {
							xCoord = -1;
							lastGazeLeft = true;
						}

						Random r = new Random();
						double y = r.nextDouble() / 8d;
						boolean up = r.nextBoolean();
						if (up) {
							y = 1 + y;
						} else {
							y = 1 - y;
						}

						bbbml = bbbml.replace("XYZ", xCoord + "," + y + ",1");

						BMLAction.sendBML(bbbml);
						new Thread() {
							public void run() {
								try {
									Thread.sleep(3000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								gazeInProgress = false;
							}
						}.start();

					} else {
						if (!returnedGaze && !gazeInProgress) {
							returnedGaze = true;
							gazeInProgress = true;

							String interrupt = "interrupt.xml";
							returnedGaze = true;
							gazeInProgress = true;
							String bbbml = "";
							try {
								bbbml = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/" + interrupt)));
							} catch (IOException e) {
								e.printStackTrace();
							}
							bbbml = bbbml.replace("bml$REPLACE_ID", "gaze0");

							BMLAction.sendBML(bbbml);
							new Thread() {
								public void run() {
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
									gazeInProgress = false;
								}
							}.start();
						}
					}

					try {
						Thread.sleep(400);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}
		}.start();
	}
}
