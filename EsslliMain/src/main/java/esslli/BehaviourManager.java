package esslli;

import ipaaca.Initializer;
import ipaaca.LocalMessageIU;
import ipaaca.OutputBuffer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import module.DialogueManager;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import esslli.model.Behaviour;

public class BehaviourManager implements Runnable {

	public int frequency = 5;
	List<Behaviour> behaviours;
	Map<String, Integer> targetsPrio = new HashMap<String, Integer>();
	Map<String, Double> oldValues = new HashMap<String, Double>();
	OutputBuffer outBuffer = new OutputBuffer("BehaviourManager");
	public static void main(String... args) throws JDOMException, IOException {
		
		

	}

	public BehaviourManager() {
		Document doc = null;
		try {
			doc = new SAXBuilder().build("domains/esslli/generation/behaviour.xml");
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		behaviours = new ArrayList<Behaviour>();

		for (Element e : doc.getRootElement().getChildren()) {
			
			Behaviour b = new Behaviour();
			b.setName(e.getAttributeValue("name"));
			if(e.getAttributeValue("priority") != null)
				b.setPriority(Integer.parseInt(e.getAttributeValue("priority")));
			else 
				b.setPriority(0);			

			DialogueManager.setVariable(e.getAttributeValue("name") + "-priority", b.getPriority());
			
			for (Element e2 : e.getChildren()) {
				if(e2.getName().equals("source")) {
					b.setSource(e2.getAttributeValue("name"));
					DialogueManager.setVariable(e2.getAttributeValue("name"), 0d);
				} else if(e2.getName().equals("target")) {
					b.getValues().put(e2.getAttributeValue("name"), Double.parseDouble(e2.getAttributeValue("weight")));
					b.getTargets().add(e2.getAttributeValue("name"));
				}
			}
			behaviours.add(b);
		}
		
	}

	public void readXML() {

		try {
			Document doc = new SAXBuilder().build("behaviour.xml");

			for (Element behaviour : doc.getRootElement().getChildren()) {
				for (Element e : behaviour.getChildren()) {
					System.out.println(e);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void run() {

		Initializer.initializeIpaacaRsb();
		long calcTime = 0;
		
		while (true) {
			
			if(DialogueManager.system != null) {
				calcTime = createBehaviourBlock();
			}
			
			try {
				if(calcTime > (1000 / frequency))
					calcTime = (1000 / frequency) - 1;
				Thread.sleep((1000 / frequency) - calcTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private long createBehaviourBlock() {
		long a = System.currentTimeMillis();

		Map<String, Double> pendingValues = new HashMap<String, Double>();
		Map<String, Double> newValues = new HashMap<String, Double>();

		double source = 0;
		double value = 0;
		List<String> parts;
		String bmlHead, mid, bmlTail;		

		LocalMessageIU localIU = new LocalMessageIU();
		List<String> blockedTargets = new ArrayList<String>();
		List<Behaviour> removedBehaviours = new ArrayList<Behaviour>();
				
		for(Behaviour b : behaviours) {
			if(!DialogueManager.system.getContent(b.getName() + "-priority").getBest().toString().equals("None")) {
				b.setPriority(Integer.parseInt(DialogueManager.system.getContent(b.getName() + "-priority").getBest().toString()));
			}
		}
		
		Collections.sort(behaviours);		
				
		Iterator<Behaviour> iter = behaviours.iterator();
		
		while(iter.hasNext()) {
			Behaviour b = iter.next();
			boolean keep = true;
						
			for(String t : b.getTargets()) {
				if(blockedTargets.contains(t)) {
					keep = false;
					removedBehaviours.add(b);
					iter.remove();
					break;
				}
			}
			if(keep) {
				blockedTargets.addAll(b.getTargets());
			}
		}
				
		for (Behaviour b : behaviours) {

			source = Double.parseDouble(DialogueManager.system.getContent(b.getSource()).getBest().toString());
						
			for (String target : b.getTargets()) {
				if (this.targetsPrio.containsKey(target)) {
					if (this.targetsPrio.get(target) > b.getPriority()) {
						pendingValues.clear();
						break;
					}
				}

				value = source * b.getValue(target);
				
				if (b.getPriority() != null) {
					this.targetsPrio.put(target, b.getPriority());
				}
				pendingValues.put(target, value);
			}
			newValues.putAll(pendingValues);
			pendingValues.clear();
		}
		
		
		
		behaviours.addAll(removedBehaviours);
		
		bmlHead = "<bml xmlns=\"http://www.bml-initiative.org/bml/bml-1.0\"  xmlns:bmlt=\"http://hmi.ewi.utwente.nl/bmlt\""
				+ "  id=\"AUTO\"><bmlt:facekeyframe type=\"MORPH\" id=\"fk1\">"
				+ "\n<FaceInterpolator parts=\"";
		
		parts = new ArrayList<String>(newValues.keySet());

		for(int i = 0; i < parts.size(); i++) {
			bmlHead += parts.get(i) + " ";
		}
		
		mid = "\">\n0.00 ";
		for(String s : parts) {
			if(!oldValues.containsKey(s)) {
				mid += "-999 ";
			} else {
				// TODO SKLETT change when ready
				mid += oldValues.get(s) + " ";
//				mid += 0 + " ";
			}
		}
		mid += "\n";
		
		mid += (1d/frequency) + " ";
		for(String s : parts) {
			mid += newValues.get(s) + " ";
		}
		mid += "\n";
		
		bmlTail = "</FaceInterpolator></bmlt:facekeyframe></bml>";

		oldValues = newValues;
				
		String bml = bmlHead + mid + bmlTail;
		localIU.setCategory("realizerRequest");
		localIU.getPayload().put("request", bml);
		localIU.getPayload().put("type", "bml");

		outBuffer.add(localIU);
		localIU = new LocalMessageIU();
		localIU.setCategory("nvbg");
		for(String s : parts) {
			localIU.getPayload().put(s, String.valueOf(newValues.get(s)));
		}

		outBuffer.add(localIU);
		
		
		long b = System.currentTimeMillis();
		return (b - a);
	}

}
