package esslli;

import ipaaca.LocalMessageIU;
import ipaaca.OutputBuffer;

import java.io.IOException;

import module.NLGModule;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import app.Main;

public class TurnTakingManager implements Runnable {

	/**
	 * offset to user silence after which the system tries to react
	 */
	private int delay = 500;
	/**
	 * offset to user silence after which the system forces itself to react
	 */
	public int silenceTolerance = 10000;

	public boolean interruptAllowed = false;

	public long lastUserSpeechEnd = Long.MAX_VALUE;
	public long lastUserSpeechStart = Long.MAX_VALUE;

	public boolean textBasedFloor = false;

	public OutputBuffer outBuffer = new OutputBuffer("TurnTakingManager");

	public NLGModule nlg;

	boolean firstTurn = true;

	public TurnTakingManager() {
		Document doc = null;
		try {
			doc = new SAXBuilder().build("domains/esslli/turntaking/turntaking.xml");
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (Element e : doc.getRootElement().getChildren()) {
			if (e.getAttributeValue("name").equals("delay"))
				delay = Integer.parseInt(e.getAttributeValue("value"));
			else if (e.getAttributeValue("name").equals("silenceTolerance"))
				silenceTolerance = Integer.parseInt(e.getAttributeValue("value"));
		}
	}

	private void evaluateFloor() {

		// FLAG: TurnTaking decision

		// this is true if the user is speaking
		if (lastUserSpeechEnd == Long.MAX_VALUE) {
			return;
		}

		if (firstTurn) {
			firstTurn = false;
		}

		// delay from tt-config
		if (System.currentTimeMillis() - lastUserSpeechEnd > delay) {

			synchronized (this) {
				// no action is planned
				while (nlg.plannedAction == null) {
					try {
						// silencetolerance from tt-config
						long timeout = silenceTolerance - (System.currentTimeMillis() - lastUserSpeechEnd);

						LocalMessageIU interruptIU = new LocalMessageIU();
						interruptIU.setCategory("tt");
						interruptIU.getPayload().put("floorAvailable", "1");
						interruptIU.getPayload().put("timeout", String.valueOf(timeout));
						if (timeout >= 0)
							outBuffer.add(interruptIU);

						if (timeout < 0)
							timeout = 0;

						if (timeout > 0)
							this.wait(timeout);

						if (EsslliContext.lastSystemSpeechTime != Long.MAX_VALUE
								&& System.currentTimeMillis() - EsslliContext.lastSystemSpeechTime < silenceTolerance) {

							timeout = silenceTolerance
									- (System.currentTimeMillis() - EsslliContext.lastSystemSpeechTime);

							if (timeout > silenceTolerance)
								timeout = silenceTolerance;

							Thread.sleep(1000);
							evaluateFloor();
							return;
						}

						if (System.currentTimeMillis() - lastUserSpeechEnd < silenceTolerance) {

							timeout = silenceTolerance - (System.currentTimeMillis() - lastUserSpeechEnd);

							if (timeout > silenceTolerance)
								timeout = silenceTolerance;

							Thread.sleep(200);
							evaluateFloor();
							return;
						}

						// time is up, use default action
						if (silenceTolerance - (System.currentTimeMillis() - lastUserSpeechEnd) < 0) {
							if (nlg.plannedAction == null) {
								nlg.plannedAction = "default";
							}
						}

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			nlg.executeAction(nlg.plannedAction);
			lastUserSpeechEnd = Long.MAX_VALUE;

		} else {
			long timeout = (System.currentTimeMillis() - lastUserSpeechEnd) + 1;
			if (EsslliListener.beforeFirstVad) {
				timeout = 10000 - (System.currentTimeMillis() - EsslliListener.startTime);
			}

			if (timeout < 100)
				timeout = 100;
			if (timeout > silenceTolerance)
				timeout = silenceTolerance;

			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			evaluateFloor();
		}
	}

	@Override
	public void run() {
		evaluateFloor();
	}

}
