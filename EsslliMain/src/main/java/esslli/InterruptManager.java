package esslli;

import ipaaca.LocalMessageIU;
import ipaaca.OutputBuffer;
import module.DialogueManager;
import module.NLGModule;
import sandbox.BMLAction;

public class InterruptManager implements Runnable {

	public NLGModule nlg;

	static OutputBuffer outBuffer = new OutputBuffer("InterruptManager");

	/**
	 * decisions per seconds (Hz)
	 */
	public int frequency = 20;
	
	public static boolean interruptBlock = false;

	public InterruptManager(NLGModule nlg) {
		this.nlg = nlg;
	}

	public boolean getInterruptDecision() {

		// FLAG: Interrupt Decision
		boolean result = DialogueManager.system.getContent("interrupt").getBest().toString().equals("true");
		DialogueManager.setVariable("interrupt", "false");			
		return result;
	}

	public static void handleInterrupt() {
		
		if(interruptBlock)
			return;

		// FLAG: Interrupt Handling (User -> Agent), gets called from VAD
		String interrupt = NLGModule.interruptString;
		interrupt = interrupt.replace("$REPLACE_ID", String.valueOf((BMLAction.id - 1)));

		String interrupt2 = NLGModule.interruptString;
		interrupt2 = interrupt2.replace("$REPLACE_ID", String.valueOf((BMLAction.id - 2)));

		LocalMessageIU interruptIU = new LocalMessageIU();

		interruptIU.setCategory("realizerRequest");
		interruptIU.getPayload().put("request", interrupt);
		interruptIU.getPayload().put("type", "bml");
		outBuffer.add(interruptIU);

		LocalMessageIU interruptIU2 = new LocalMessageIU();
		interruptIU2.setCategory("realizerRequest");
		interruptIU2.getPayload().put("request", interrupt2);
		interruptIU2.getPayload().put("type", "bml");
		outBuffer.add(interruptIU2);

		EsslliContext.systemSpeaking = false;

	}

	public void interrupt() {

		interruptBlock = true;
		// FLAG: Interrupt Behaviour (Agent -> User)
		nlg.executeAction(nlg.plannedAction);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		interruptBlock = false;
		
	}

	@Override
	public void run() {

		while (true) {

			long start = System.currentTimeMillis();

			if (DialogueManager.system != null) {
				if (getInterruptDecision()) {
					interrupt();
				}
			}
			long end = System.currentTimeMillis();

			try {
				long calcTime = (end - start);
				if (calcTime > 1000 / frequency)
					calcTime = (1000 / frequency) - 1;

				Thread.sleep(1000 / frequency - calcTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}
