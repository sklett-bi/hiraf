package esslli.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Behaviour implements Comparable<Behaviour>{

	private String name;
	private String source;
	private List<String> targets = new ArrayList<String>();
	private Integer priority;
	private Map<String, Double> values = new HashMap<String, Double>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue(String target) {
		return values.get(target);
	}

	public Integer getPriority() {
		return priority;
	}

	public String getSource() {
		return source;
	}

	public List<String> getTargets() {
		return this.targets;
	}

	public Map<String, Double> getValues() {
		return values;
	}

	public void setValues(Map<String, Double> values) {
		this.values = values;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setTargets(List<String> targets) {
		this.targets = targets;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@Override
	public int compareTo(Behaviour b) {
		if(this.priority > b.priority)
			return -1;
		else if(this.priority < b.priority)
			return 1;
		
		return 0;
	}
	
	@Override
	public String toString() {
		String result = "";
		
		result += name + ":" + priority;
		
		return result;
	}

}
