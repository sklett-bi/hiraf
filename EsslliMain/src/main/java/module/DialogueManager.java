package module;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.SlotIU;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import module.opendial.ConfigurableModule;
import opendial.DialogueSystem;
import opendial.bn.distribs.CategoricalTable;
import opendial.bn.values.Value;
import opendial.datastructs.Assignment;
import opendial.domains.Domain;
import opendial.readers.XMLDomainReader;
import sium.nlu.stat.DistRow;
import bn.value.IUVal;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Boolean;
import edu.cmu.sphinx.util.props.S4ComponentList;
import edu.cmu.sphinx.util.props.S4String;

public class DialogueManager extends IUModule {

	@S4String(defaultValue = "")
	public final static String DOMAIN_PROP = "domainFile";

	@S4Boolean(defaultValue = true)
	public final static String SHOW_GUI = "showGUI";

	@S4ComponentList(type = ConfigurableModule.class)
	public final static String OPENDIAL_MODULES = "openDialModules";
	public List<ConfigurableModule> openDialModules;

	public static DialogueSystem system;

	public static void setVariable(String variable, String value) {
		system.addContent(variable, value);
	}

	public static void setVariable(String variable, double value) {
		system.addContent(variable, value);
	}

	public static String getVariable(String variable) {
		String result = "";
		
		if(system.getState().hasNode(variable) && system.getContent(variable) != null)
			result = system.getContent(variable).getBest().toString();
		
		return result;
	}

	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);

		Domain domain = XMLDomainReader.extractDomain(ps.getString(DOMAIN_PROP));
		system = new DialogueSystem(domain);
		system.getSettings().showGUI = ps.getBoolean(SHOW_GUI);

		openDialModules = ps.getComponentList(OPENDIAL_MODULES, ConfigurableModule.class);
		if (openDialModules != null) {
			for (ConfigurableModule module : openDialModules) {
				system.attachModule(module);
			}
		}
		system.startSystem();
	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius, List<? extends EditMessage<? extends IU>> edits) {

		for (EditMessage<? extends IU> edit : edits) {
			IU iu = edit.getIU();
			if (iu instanceof SlotIU)
				updatePartiallyObservedState(iu.getClass().getSimpleName(), (SlotIU) iu, edit.getType().toString());
			else
				updateObservedState(iu.getClass().getSimpleName(), iu, edit.getType().toString());
		}
		// after all of the state changes have been made, trigger the update in
		// the OpenDial BN
		system.update();
	}

	private void updatePartiallyObservedState(String attribute, SlotIU iu, String type) {
		system.addPartialContent(new Assignment("EditType", type));
		// Add partially-observed variables
		Map<Value, Double> table = new HashMap<Value, Double>();

		for (DistRow<String> row : iu.getDistribution().getDistribution()) {
			table.put(new IUVal(iu, row.getProbability()), row.getProbability());
		}
		CategoricalTable t = new CategoricalTable(attribute, table);
		system.addContent(t);
	}

	private void updateObservedState(String attribute, IU iu, String type) {
		system.addPartialContent(new Assignment("EditType", type));
		system.addPartialContent(new Assignment(attribute, new IUVal(iu, 1.0)));
	}

}
