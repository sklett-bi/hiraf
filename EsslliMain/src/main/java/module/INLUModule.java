package module;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.SlotIU;
import ipaaca.LocalMessageIU;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import model.Constants;
import model.DomainModel;
import model.Frame;
import model.db.Domain;
import model.iu.FrameIU;
import util.DeepCopy;
import util.SessionTimeout;
import app.Main;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4Integer;
import edu.cmu.sphinx.util.props.S4String;
import esslli.EsslliListener;

public class INLUModule extends IUModule {

	@S4Component(type = TreeModule.class)
	public final static String TREE_MODULE = "module";
	protected TreeModule tree;
	public boolean start = true;

	@S4String(defaultValue = "test")
	public final static String DOMAIN = "domain";

	@S4Integer(defaultValue = 60000)
	public final static String TIMEOUT = "timeout";

	public static DomainModel model;

	private String currentIntent;

	// deactivated
	List<String> blacklist = new ArrayList<String>();

	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {

		currentIntent = Constants.ROOT_NAME;
		Domain db = new Domain();
		try {
			db.setDomain(ps.getString(DOMAIN));
			model = new DomainModel(db, ps.getString(DOMAIN));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		SessionTimeout.setVariables(this, ps.getInt(TIMEOUT));
		super.newProperties(ps);
		tree = (TreeModule) ps.getComponent(TREE_MODULE);
	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius, List<? extends EditMessage<? extends IU>> edits) {

		// FLAG: NLU leftBuffer

		if(start)
			start = false;		

		SessionTimeout.getInstance().reset();

		for (EditMessage<? extends IU> edit : edits) {

			String word = edit.getIU().toPayLoad().toLowerCase();

			if (word.contains("?")) {
				EsslliListener.tt.textBasedFloor = false;
				word = word.replace("?", "");
			} else if (!EsslliListener.tt.textBasedFloor && !Main.asrActive) {
				EsslliListener.tt.textBasedFloor = true;
			}

			switch (edit.getType()) {

			case ADD:

				LocalMessageIU localIU = new LocalMessageIU();
				localIU.getPayload().put("word", word);
				localIU.setCategory("asrword");
				Main.outBuffer.add(localIU);

				if (word.equals(Constants.RESET_KEYWORD)) {
					resetSession();
					continue;
				}

				checkContext();
				model.addIncrement(word);
				update();
				break;
			default:
				break;
			}
		}
	}

	public void resetSession() {
		model.newUtterance();
		tree.initDisplay(true, true);
		tree.logReset();
	}

	protected void checkContext() {

		if (blacklist.size() == 0) {
			blacklist.add("the");
			blacklist.add("to");
			blacklist.add("how");
			blacklist.add("a");
			blacklist.add("you");
			blacklist.add("one");
			blacklist.add("i");
			blacklist.add("i'm");
			blacklist.add("and");
			blacklist.add("or");
			blacklist.add("one");
			blacklist.add("i");
			blacklist.add("you");
		}

		if (!this.currentIntent.equals(tree.getCurrentIntent())) {
			this.currentIntent = tree.getCurrentIntent();
			model.updateContext(this.currentIntent);
		}
	}

	public void update() {
		try {

			// FLAG: NLU rightBuffer
			List<EditMessage<? extends IU>> edits = new ArrayList<EditMessage<? extends IU>>();
			Frame predictedFrame = model.getPredictedFrame();
			for (String intent : predictedFrame.getIntents()) {
				FrameIU frameIU = new FrameIU(predictedFrame);
				SlotIU slotIUIntent = frameIU.getSlotIUForIntent(intent);

				if (slotIUIntent == null || slotIUIntent.getDistribution() == null
						|| slotIUIntent.getDistribution().isEmpty())
					continue;

				String concept = slotIUIntent.getDistribution().getArgMax().getEntity().toLowerCase();

				if (slotIUIntent.getConfidence() > 0.2) {
					LocalMessageIU iu = new LocalMessageIU("inlu");
					iu.getPayload().put("confidence", String.valueOf(slotIUIntent.getConfidence()));
					iu.getPayload().put("intent", intent);
					iu.getPayload().put("concept", concept);
					Main.outBuffer.add(iu);
					
					SlotIU slotIUConcept = (SlotIU) DeepCopy.copy(slotIUIntent);
					slotIUConcept.setName(concept);
					edits.add(new EditMessage<SlotIU>(EditType.ADD, slotIUConcept));
				}
			}

			super.rightBuffer.setBuffer(edits);
			super.notifyListeners();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public Frame getFilledFrame() {
		return tree.getFilledFrame();
	}

	public boolean isInFunction() {
		return tree.isInFunction();
	}

}
