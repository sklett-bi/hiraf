package module;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.SlotIU;
import ipaaca.LocalMessageIU;
import ipaaca.Payload;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import sandbox.BMLAction;
import sandbox.QuizBowlEntry;
import sandbox.QuizBowlParser;
import app.Main;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import esslli.EsslliContext;
import esslli.EsslliListener;
import esslli.InterruptManager;

public class NLGModule extends IUModule {

	private PropertySheet propertySheet;

	public String plannedAction = "default";

	public static String interruptString = "";

	private Map<String, String> bmlMap = new HashMap<String, String>();
	private Map<String, String> utteranceMap = new HashMap<String, String>();
	private Map<String, String> methodMap = new HashMap<String, String>();

	
	/**
	 * domain specific variables
	 */
	private List<QuizBowlEntry> quizEntries;
	private QuizBowlEntry activeEntry;
	private int chunkIndex = 0;

	public void init() {		
				
		// interrupt possible breathing		
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			String breathInterruptbml = "";
			try {
				breathInterruptbml = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/interrupt.xml")));
			} catch (IOException e) {
				e.printStackTrace();
			}
			breathInterruptbml = breathInterruptbml.replace("$REPLACE_ID", "breathing");	
			BMLAction.sendBML(breathInterruptbml);
		}));
		
		
		// start breathing
		
		String bb = "blink_breath.xml";

		String bbbml = "";
		try {
			bbbml = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/" + bb)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		BMLAction.sendBML(bbbml);
		
		// end

		quizEntries = QuizBowlParser.createEntries();

		Document doc = null;

		try {
			doc = new SAXBuilder().build("domains/esslli/generation/nlg.xml");
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (Element e1 : doc.getRootElement().getChildren()) {
			for (Element e2 : e1.getChildren()) {
				if (e2.getAttributeValue("utterance") != null) {
					utteranceMap.put(e1.getAttributeValue("value"), e2.getAttributeValue("utterance"));
				} else if (e2.getAttributeValue("method") != null) {
					methodMap.put(e1.getAttributeValue("value"), e2.getAttributeValue("method"));
				} else if (e2.getAttributeValue("bml") != null) {
					String fileName = e2.getAttributeValue("bml");

					String bml = "";
					try {
						bml = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/" + fileName)));
					} catch (IOException e) {
						e.printStackTrace();
					}

					bmlMap.put(e1.getAttributeValue("value"), bml);
				}
			}
		}
		try {
			interruptString = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/" + "interrupt.xml")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void planAction(String action) {

		boolean interrupt = DialogueManager.getVariable("interrupt").equals("true");
		
		if(EsslliContext.systemSpeaking) {
			return;
		}
		
		if (EsslliListener.tt.textBasedFloor || interrupt) {
			executeAction(action);
			DialogueManager.setVariable("interrupt", "false");	
		} else {
			LocalMessageIU localIU = new LocalMessageIU();
			localIU.getPayload().put("plannedAction", action);
			localIU.setCategory("nlg");
			Main.outBuffer.add(localIU);
			plannedAction = action;

			synchronized (EsslliListener.tt) {
				// FLAG: TurnTaking notifier
				EsslliListener.tt.notify();
			}
		}
	}

	public void executeAction(String action) {

		if(EsslliContext.systemSpeaking) {
			return;
		}		
		EsslliContext.systemSpeaking = true;
		EsslliListener.returnedGaze = true;
		EsslliListener.gazeInProgress = true;	

		String interrupt = "interrupt.xml";
		
		String bbbml = "";
		try {
			bbbml = new String(Files.readAllBytes(Paths.get("domains/esslli/bml/" + interrupt)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		bbbml = bbbml.replace("bml$REPLACE_ID", "gaze0");	
		
		BMLAction.sendBML(bbbml);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				EsslliListener.gazeInProgress = false;
			}
		}.start();
		
		if (action == null || action.equals("")) {
			action = "default";
		}
		String utterance = "";
		String paramName = "";
		Object args = null;
		if (utteranceMap.containsKey(action)) {
			utterance = utteranceMap.get(action);
		} else if (bmlMap.containsKey(action)) {
			utterance = bmlMap.get(action);
		} else if (methodMap.containsKey(action)) {
			String s = methodMap.get(action);
			Class<?> nlgClass = this.getClass();
			for (Method m : nlgClass.getMethods()) {
				String[] split = s.split("\\(");
				if (m.getName().equals(split[0])) {

					boolean withParam = false;
					if (!split[1].equals(")")) {
						withParam = true;
						paramName = split[1].replace(")", "");
						args = DialogueManager.system.getContent(paramName).getBest().toString();
					}
					try {
						if (withParam)
							utterance = (String) m.invoke(this, args);
						else
							utterance = (String) m.invoke(this, new Object[0]);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		} 

		LocalMessageIU localIU = new LocalMessageIU();
		localIU.getPayload().put("executedAction", action);
		if (utterance.length() > 0)
			localIU.getPayload().put("utterance", utterance);

		if (args != null) {
			localIU.getPayload().put(paramName, (String) args);
		}

		localIU.setCategory("nlg");

		if (utterance.contains("<bml")) {
			localIU.getPayload().remove("utterance");
			localIU.getPayload().put("bml", action);
		}

		Main.outBuffer.add(localIU);

		if (utterance.contains("<bml")) {
			BMLAction.sendBML(utterance);
		} else
			BMLAction.createAndSendBML(utterance);

		plannedAction = null;
		// FLAG: this resets the evidence gathered by the iNLU
		INLUModule.model.newUtterance();
	}

	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {

	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius, List<? extends EditMessage<? extends IU>> edits) {

		for (EditMessage<? extends IU> edit : edits) {

			SlotIU decisionIU = (SlotIU) edit.getIU();

			if (edit.getIU().groundedIn().isEmpty())
				continue;

			SlotIU slotIU = (SlotIU) edit.getIU().groundedIn().get(0);

			String concept = slotIU.getDistribution().getArgMax().getEntity().toLowerCase();
			String intent = slotIU.getName();
			Double confidence = slotIU.getConfidence();
			String decision = decisionIU.getDistribution().getArgMax().getEntity();

			LocalMessageIU localIU = new LocalMessageIU();
			localIU.getPayload().put(intent, concept);
			localIU.getPayload().put("confidence", confidence.toString());
			localIU.setCategory("dm");

			if (!decision.equals("default")) {
				if (decisionIU.getName().equals("commit")) {
					LocalMessageIU commitIU = new LocalMessageIU("commit");
					commitIU.getPayload().put("commit", decision);
					Main.outBuffer.add(commitIU);
				} else if (decisionIU.getName().equals("feedback")) {
					sendFeedbackBML(decision);
					LocalMessageIU feedbackIU = new LocalMessageIU("feedback");
					feedbackIU.getPayload().put("feedback", decision);
					Main.outBuffer.add(feedbackIU);
				} else {
					localIU.getPayload().put("decision", decision);
					if (intent.equals(concept)) {
						localIU.getPayload().put("entity", concept);
						localIU.getPayload().remove(intent);
						Main.outBuffer.add(localIU);
					}
				}
			}

			Payload payload = localIU.getPayload();

			if (payload.containsKey("decision")) {

				planAction(decision);
			}

			update();
		}
	}

	public void sendFeedbackBML(String name) {

		InterruptManager.handleInterrupt();
		InterruptManager.interruptBlock = true;
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				InterruptManager.interruptBlock = false;
			}
		}.start();
		
		String bml = bmlMap.get(name);
	
		BMLAction.sendBML(bml);
	
	}

	public void update() {
		notifyListeners();
	}

	public PropertySheet getPropertySheet() {
		return propertySheet;
	}

	public void setPropertySheet(PropertySheet propertySheet) {
		this.propertySheet = propertySheet;
	}

	// domain specific methods start here
	public String startQuestion() {

		chunkIndex = 0;
		Random r = new Random();
		int index = r.nextInt(quizEntries.size());

		activeEntry = quizEntries.get(index);
		return activeEntry.questionChunks.get(chunkIndex);

	}

	public String continueQuestion() {

		if (chunkIndex < activeEntry.questionChunks.size() - 1)
			chunkIndex++;

		return activeEntry.questionChunks.get(chunkIndex);

	}

	public String repeat() {

		return activeEntry.questionChunks.get(chunkIndex);

	}

	public String offerAnswer(String question) {

		String answer = quizEntries.get(Integer.parseInt(question.replace("question", "")) - 1).answer;

		return "is " + answer + " correct?";
	}

	public String checkAnswer(String answer) {

		String utterance = "";
		if (activeEntry.id == Integer.parseInt(answer.replace("answer", ""))) {
			utterance = "nice job!";
		} else {
			utterance = "no, try again";
		}
		return utterance;

	}

}
