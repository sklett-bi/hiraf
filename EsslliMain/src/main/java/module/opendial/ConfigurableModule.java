package module.opendial;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.SlotIU;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import opendial.DialogueState;
import opendial.bn.distribs.CategoricalTable;
import opendial.bn.values.Value;
import opendial.modules.Module;
import sium.nlu.stat.Distribution;
import bn.value.IUVal;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4String;

public class ConfigurableModule extends IUModule implements Module {
	
	@S4String(defaultValue = "")
	public final static String EFFECT_VARIABLE = "effectVariable";	
	private String effectVariable;
	
	private LinkedList<SlotIU> addedIUStack;
	private DialogueState previousState;
	private Collection<String> previousUpdatedVars;
	private boolean isPaused;
	
	public static DialogueState stateStart;
	public static DialogueState statePending;
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
//		the effect variable is the one that is monitored by this module; changes to it are put on the right buffer
		setEffectVariable(ps.getString(EFFECT_VARIABLE)); 
		addedIUStack = new LinkedList<SlotIU>();
	}

	@Override
	public void start() {

	}

	@Override
	public void trigger(DialogueState state, Collection<String> updatedVars) {

		
//		if(stateStart == null)
//			stateStart = (DialogueState) state.copy();
//		
//		state = (DialogueState) stateStart.copy();
		
		ArrayList<EditMessage<? extends IU>> newEdits = new ArrayList<EditMessage<? extends IU>>();

//		System.out.println("update: " + updatedVars);
		
		if (!isPaused() && updatedVars.contains(getEffectVariable())) {
//			get the distribution from the network for this particular variable
			CategoricalTable table = state.queryProb(getEffectVariable()).toDiscrete();
			String editType = state.queryProb("EditType").toDiscrete().getBest().toString();
			
//			get the grounded in links from the previous step (the variables that triggered the change in the effect variable)
			ArrayList<IU> grin = getGroundedInIUs();
//			put the distribution from OpenDial into a SlotIU distribution
			Distribution<String> outDist = tableToDistribution(table);
			
			if ("ADD".equals(editType)) {
				
				SlotIU siu = new SlotIU(getEffectVariable(), outDist);
				siu.groundIn(grin);
				IU topIU = getTopIU();
				siu.setSameLevelLink(topIU);
				if (topIU != null) {
					topIU.addNextSameLevelLink(siu);
				}
				
				newEdits.add(new EditMessage<IU>(EditType.ADD, siu));
				pushNewlyAddedIU(siu);
			}
			else if ("REVOKE".equals(editType)) {
				for (IU giu : grin) {
					for (IU sliu : giu.grounds()) {
						newEdits.add(new EditMessage<IU>(EditType.REVOKE, sliu));	
					}
				}
			}

			else if ("COMMIT".equals(editType) && allGrinsAreCommitted(grin)) {
				for (IU giu : grin) {
					for (IU sliu : giu.grounds())
					newEdits.add(new EditMessage<IU>(EditType.COMMIT, sliu));
				}
			}
		}
		
//		these keep track of the most recent step, which is what is grounded into when an effect variable is changed
		setPreviousState(state.copy());
		setPreviousUpdatedVars(new ArrayList<String>(updatedVars));
		
		statePending = state;
		
		
		if (!newEdits.isEmpty()) {
			rightBuffer.setBuffer(newEdits);
			super.notifyListeners();
		}
	}

	private Distribution<String> tableToDistribution(CategoricalTable table) {
		Distribution<String> outDist = new Distribution<String>();
		for (Value value : table.getValues()) {
			outDist.addProbability(value.toString(), table.getProb(value));
		}		
		return outDist;
	}
	
	private boolean allGrinsAreCommitted(ArrayList<IU> grin) {
		for (IU iu : grin)
			if (!iu.isCommitted()) return false;
		return true;
	}

	private ArrayList<IU> getGroundedInIUs() {
//		get the groundedIn links from OpenDial's BNetwork. At the moment, assume it's the variables triggered in the previous state
		ArrayList<IU> grin = new ArrayList<IU>();
		for (String prevUpdatedVar : getPreviousUpdatedVars()) {
			for (Value value: getPreviousState().queryProb(prevUpdatedVar).getValues()) {
				if (value instanceof IUVal) {
					IUVal iuvalue = (IUVal) value;
					grin.add(iuvalue.getIU());
				}
			}
		}
		return grin;
	}

	@Override
	public void pause(boolean toPause) {
		setPaused(isPaused);
	}

	@Override
	public boolean isRunning() {
		return !isPaused();
	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius, List<? extends EditMessage<? extends IU>> edits) {
		throw new RuntimeException("This left buffer updated should never be called.");
	}

	public String getEffectVariable() {
		return effectVariable;
	}

	public void setEffectVariable(String effectVariable) {
		this.effectVariable = effectVariable;
	}

	public DialogueState getPreviousState() {
		return previousState;
	}

	public void setPreviousState(DialogueState previousState) {
		this.previousState = previousState;
	}

	public Collection<String> getPreviousUpdatedVars() {
		return previousUpdatedVars;
	}

	public void setPreviousUpdatedVars(Collection<String> previousUpdatedVars) {
		this.previousUpdatedVars = previousUpdatedVars;
	}

	public boolean isPaused() {
		return isPaused;
	}

	public void setPaused(boolean isPaused) {
		this.isPaused = isPaused;
	}
	
	public SlotIU popTopIU() {
		return addedIUStack.pop();
	}

	public SlotIU getTopIU() {
////		if (addedIUStack.isEmpty()) return new SlotIU(); 
//		if (!addedIUStack.containsKey(type)) return null;
		return addedIUStack.peek();
	}

	public void pushNewlyAddedIU(SlotIU mostRecentlyAddedIU) {
		
//		System.out.println("pushing: " + mostRecentlyAddedIU.deepToString());
//		if (!addedIUStack.containsKey(type))
//			addedIUStack.put(type, new LinkedList<SlotIU>());
		addedIUStack.push(mostRecentlyAddedIU);
	}

}
