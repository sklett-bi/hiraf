package app;

import inpro.apps.SimpleReco;
import inpro.apps.SimpleText;
import inpro.apps.util.RecoCommandLineParser;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.TextBasedFloorTracker;
import inpro.incremental.source.GoogleASR;
import inpro.incremental.source.IUDocument;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import ipaaca.Initializer;
import ipaaca.OutputBuffer;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.UnsupportedAudioFileException;

import jetty.DiaTreeSocket;
import module.NLGModule;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import util.ClientUtils;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.S4Component;
import esslli.BehaviourManager;
import esslli.EsslliListener;

public class Main {

	/**
	 * this variable controls whether speech input is recognized or not
	 */
	public static boolean asrActive = false;
	
	public static boolean openBrowser = false;

	static Logger log = Logger.getLogger("root");
	public static OutputBuffer outBuffer = new OutputBuffer("main");

	@S4Component(type = DiaTreeSocket.class)
	public final static String DIATREE_SOCKET = "diatree";

	@S4Component(type = TextBasedFloorTracker.class)
	public final static String PROP_FLOOR_MANAGER = "textBasedFloorTracker";

	GoogleASR webSpeech;

	private static IUDocument iuDocument;

	private static String googleApiKey = "";

	List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();

	private void run() throws InterruptedException, PropertyException, IOException, UnsupportedAudioFileException {

		log.setLevel(Level.OFF);

		Initializer.initializeIpaacaRsb();

		EsslliListener listener = new EsslliListener();
		ConfigurationManager cm = new ConfigurationManager(new File("src/main/java/config/config.xml").toURI().toURL());

		NLGModule nlg = (NLGModule) cm.lookup("nlg");
		nlg.init();
		EsslliListener.nlg = nlg;

		Thread t = new Thread(listener);
		t.start();

		if (googleApiKey.equals("")) {
			googleApiKey = cm.getPropertySheet("googleASR").getString("apiKey");
		}

		if (asrActive) {
			System.out.println("using google speech api key: " + googleApiKey);
			System.out.println("starting google asr... wait until calibration is finished");
		}

//		VoiceActivityDetector vad = (VoiceActivityDetector) cm.lookup("vad");

//		AdvancedDiaTreeCreator creator = new AdvancedDiaTreeCreator((DiaTreeSocket) cm.lookup(DIATREE_SOCKET));
//		JettyServer jetty = new JettyServer(creator);
//		CustomFunctionRegistry cfr = (CustomFunctionRegistry) cm.lookup("registry");

		// for Google ASR
		webSpeech = (GoogleASR) cm.lookup("googleASR");
		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] { "-M", "-G", googleApiKey });
//		
//		System.out.println(rclp.getConfigURL());
//		SimpleReco simpleReco = new SimpleReco(cm, rclp);
//		
//		simpleReco.recognizeInfinitely();
		
		if (asrActive)
			startGoogleASR(cm, rclp);

		ClientUtils.openNewClient();

		TextBasedFloorTracker textBasedFloorTracker = (TextBasedFloorTracker) cm.lookup(PROP_FLOOR_MANAGER);
		iuDocument = new IUDocument();
		iuDocument.setListeners(webSpeech.iulisteners);
		SimpleText.createAndShowGUI(webSpeech.iulisteners, textBasedFloorTracker);

		BehaviourManager bm = new BehaviourManager();
		Thread bmT = new Thread(bm);
		bmT.start();

	}

	private void startGoogleASR(ConfigurationManager cm, RecoCommandLineParser rclp) {
		new Thread() {
			public void run() {

				try {
					SimpleReco simpleReco = new SimpleReco(cm, rclp);
					while (true) {
						try {

							new Thread() {
								public void run() {
									try {
										simpleReco.recognizeOnce();
									} catch (PropertyException e) {
										e.printStackTrace();
									}

								}
							}.start();

							Thread.sleep(10000);
							webSpeech.shutdown();
							// simpleReco.shutdownMic();
						}

						catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

				} catch (PropertyException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (UnsupportedAudioFileException e1) {
					e1.printStackTrace();
				}
			}
		}.start();

	}

	public static void main(String[] args) {

			try {
				new Main().run();
			} catch (PropertyException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (UnsupportedAudioFileException e) {
				e.printStackTrace();
			}
	}

	public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			// logger.debug("notifying about" + edits);
			for (PushBuffer listener : listeners) {
				listener.hypChange(null, edits);
			}
			edits = new ArrayList<EditMessage<IU>>();
		}
	}

}
