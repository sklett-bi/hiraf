/**
A GUI to manipulate (improve, mimic, parody) Mary TTS.
The TTS's unit and prosody model can be manipulated and
this can be resynthesized. For example, TTS errors can be
corrected, prosody can be improved, etc.

TODO before release:
change to Mary 4.1.1 (which supports duration and pitch adaptation with HMM voices)
	however, HMM voices have a continuous pitch track, which would be horribly annoying
Mary-Anbindung konfigurierbar machen
make audio more responsive
	abort playing/streaming if another edit is about to happen
	option to not automatically play after every edit
	use mp3 for streaming?
support different languages (english, who cares about tibetan?) 
tatsächliche Intonationskurve errechnen und auch darstellen &rarr; mphf, beschissen
	mithilfe von l2fprod (liegt aufm Desktop)
"Wussten Sie schon"-Box für unentdeckbare Features
	Grenzen und Pitchpunkte durch drag/drop verschieben
	Shift beim verschieben ändert nur diese Grenze, nicht alle folgenden
	bei Rechtsklick im Segmentteil erscheint ein Pop-Up Menü mit dem Segmente eingefügt, geändert und gelöscht werden können
	Mit den Knöpfen oben links können PHO-Dateien importiert und exportiert, sowie Audiodateien gespeichert werden
...
see also http://pascal.kgw.tu-berlin.de/expressive-speech/online/synthesis/german/en/ia-en.php
see also http://kitt.cl.uzh.ch/clab/dialogsysteme/ilap_mary/"

DONE
show a dialogue on mary-error (server not up, etc.
generate a default filename for saving
allow manual editing of label file

@author Timo Baumann
@version 0.9
 */
package extra.inpro.synthesis.visual;