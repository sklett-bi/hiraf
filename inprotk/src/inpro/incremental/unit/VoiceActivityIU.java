package inpro.incremental.unit;

public class VoiceActivityIU extends IU {
	
	/*
	 * true means that speech has started and will remain in that state until another VoiceActivityIU is sent
	 * false means that the speech has ended and will remain in that state until another VoiceActivityIU is sent
	 */

	private boolean speechStarted;
	private double startTime;
	
	public VoiceActivityIU(boolean b) {
		this.setSpeechStarted(b);
		this.setStartTime(Double.NaN);
	}
	
	public VoiceActivityIU(boolean b, double s) {
		this.setSpeechStarted(b);
		this.setStartTime(s);
	}

	private void setSpeechStarted(boolean b) {
		this.speechStarted = b;
	}

	@Override
	public String toPayLoad() {
		return  this.isSpeechStarted() ? "speech_started" : "speech_ended";  
	}

	public boolean isSpeechStarted() {
		return speechStarted;
	}
	
	public boolean isSpeechEnded() {
		return !speechStarted;
	}

	@Override
	public double startTime() {
		return startTime;
	}
	
	@Override
	public double endTime() {
		return startTime;
	}

	public void setStartTime(double startTime) {
		this.startTime = startTime;
	}


}
