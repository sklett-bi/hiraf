package inpro.incremental.source;

import inpro.annotation.Label;
import inpro.audio.FrontEndBackedAudioInputStream;
import inpro.incremental.FrameAware;
import inpro.incremental.PushBuffer;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.IUList;
import inpro.incremental.unit.SegmentIU;
import inpro.incremental.unit.TextualWordIU;
import inpro.incremental.unit.WordIU;
import inpro.util.PathUtil;
import inpro.util.TimeUtil;
import ipaaca.Initializer;
import ipaaca.LocalMessageIU;
import ipaaca.OutputBuffer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javaFlacEncoder.FLACFileWriter;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.cmu.sphinx.frontend.BaseDataProcessor;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4String;

/**
 * Incremental speech recognition based on GoogleASR
 * 
 * Uses Google's Full-Duplex Speech API (Version 1), which uses one HTTP POST
 * connection for uploading audio and one HTTP GET connection for asynchronously
 * receiving intermediate results during recognition. There's no promise of
 * intermediate results, but they do occur every once in a while.
 * 
 * Google communicates no time-alignments for recognized words so we use a
 * heuristic to estimate timings of word alignments.
 * 
 * @author casey, timo, jtwiefel
 */
public class GoogleASR extends IUSourceModule {

	/**
	 * if this is true, only the final hypothesis is used
	 */
	private boolean robust = false;
	public static boolean asrDemo = false;
	public boolean isSpeaking = false;
	public int delayThreshold = 18;
	public int volumeThresholdSilence = 40;
	public int volumeThresholdVoice = 70;
	public boolean vadCalibration = true;
	public long timeStart = 0;
	public double caliSum = 0;
	public int caliCount = 0;

	public static OutputBuffer outBuffer;

	public boolean vadActive = true;

	@S4String(defaultValue = "en-US")
	public final static String PROP_ASR_LANG = "lang";
	private String languageCode = "en-US";

	@S4String(defaultValue = "SetThisToYourGoogleApiKey")
	public final static String PROP_API_KEY = "apiKey";
	private String googleAPIkey;

	@S4String(defaultValue = "sticky")
	public final static String PROP_JSON_MODE = "jsonMode";
	private String jsonAnalysisMode;

	@S4String(defaultValue = "16000")
	public final static String PROP_SAMPLING_RATE = "samplingRate";
	private String samplingRate = "16000";

	@S4String(defaultValue = "")
	public final static String PROP_READ_JSON_DUMP = "importJSONDump";
	private URL jsonDumpInput;

	@S4String(defaultValue = "")
	public final static String PROP_WRITE_JSON_DUMP = "exportJSONDump";
	private File jsonDumpOutput;

	/** used by google as a signal that audio transmission has ended */
	private final static byte[] FINAL_CHUNK = new byte[] { '0', '\r', '\n', '\r', '\n' };
	/** size of the output buffer, how much audio to send to google at a time */
	private final static int BUFFER_SIZE = 320; // 2000 samples = 250ms; 320 =
												// 10ms
	/** that's who we pretend to be when talking with Google */
	private final static String UA_STRING = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36";

	private FrontEndBackedAudioInputStream ais = null;
	private boolean moreAudioLeft;

	public GoogleASR() {
	}

	public GoogleASR(BaseDataProcessor frontend) {
		ais = new FrontEndBackedAudioInputStream(frontend);
		iulisteners = new ArrayList<PushBuffer>(1); // avoid nullpointer
													// exceptions
	}

	public void setFrontEnd(BaseDataProcessor frontend) {
		ais = new FrontEndBackedAudioInputStream(frontend);
	}

	/** enable Sphinx configuration */
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		// iulisteners.addAll(ps.getComponentList(PROP_HYP_CHANGE_LISTENERS,
		// PushBuffer.class));
		setLanguageCode(ps.getString(PROP_ASR_LANG));
		setAPIKey(ps.getString(PROP_API_KEY));
		setSamplingRate(ps.getString(PROP_SAMPLING_RATE));
		jsonAnalysisMode = ps.getString(PROP_JSON_MODE);
		try {
			if (!"".equals(ps.getString(PROP_READ_JSON_DUMP))) {
				setImportFile(PathUtil.anyToURL(ps.getString(PROP_READ_JSON_DUMP)));
			} else {
				setImportFile(null);
			}
		} catch (MalformedURLException e) {
			throw new PropertyException(e);
		}
		if (!"".equals(ps.getString(PROP_WRITE_JSON_DUMP))) {
			setExportFile(new File(ps.getString(PROP_WRITE_JSON_DUMP)));
		} else {
			setExportFile(null);
		}
	}

	/** write audio to stream and push results to listeners of our RightBuffer */
	public void recognize() {

		if (outBuffer == null) {
			Initializer.initializeIpaacaRsb();
			outBuffer = new OutputBuffer("GoogleASR-Dup");
		}

		try {
			String pair = getPair();
			GoogleJSONListener jsonlistener;
			OutputStream upStream;
			if (jsonDumpInput == null) {
				// setup connection with Google
				HttpURLConnection upCon = getUpConnection(pair);
				// push audio to Google (on this thread)
				upStream = new DataOutputStream(upCon.getOutputStream());
				// start listening on return connection (separate thread)
				jsonlistener = new LiveJSONListener(pair);
			} else {
				// get downstream
				jsonlistener = new PlaybackJSONListener(jsonDumpInput);
				// send upstream to /dev/null
				upStream = new OutputStream() {
					@Override
					public void write(int b) throws IOException {
					}
				};
			}
			Thread listenerThread = new Thread(jsonlistener, "Google recognition thread");
			listenerThread.start();
			// write to stream
			writeToStream(upStream, ais);
			// on data end, end listening thread and tear down connection with
			// google
			Thread.sleep(400);
			jsonlistener.shutdown();
			listenerThread.join();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/** get connection to Google that we use for uploading our audio data */
	private HttpURLConnection getUpConnection(String pair) throws IOException {
		HttpURLConnection connection;

		String upstream = "https://www.google.com/speech-api/full-duplex/v1/up?" + "key=" + googleAPIkey + "&pair=" + pair + "&lang="
				+ languageCode + "&maxAlternatives=10&client=chromium&continuous&interim&output=json&xjerr=1";

		URL url = new URL(upstream);
		connection = (HttpURLConnection) url.openConnection();
		// adjust the connection
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setChunkedStreamingMode(1200);
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Transfer-Encoding", "chunked");
		connection.setRequestProperty("Content-Type", "audio/x-flac; rate=" + samplingRate);
		connection.setRequestProperty("User-Agent", UA_STRING);
		connection.setConnectTimeout(60000);
		connection.setUseCaches(false);
		return connection;
	}

	/** get connection to Google that we use for receiving JSON results */
	private HttpURLConnection getDownConnection(String pair) throws IOException {

		HttpURLConnection connection = null;
		String downstream = "https://www.google.com/speech-api/full-duplex/v1/down?pair=" + pair;
		URL url = new URL(downstream);
		connection = (HttpURLConnection) url.openConnection();
		// adjust the connection
		connection.setDoOutput(true);
		// connection.setChunkedStreamingMode(0);
		// connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent", UA_STRING);
		connection.setConnectTimeout(60000);
		connection.setUseCaches(false);
		return connection;
	}

	/** generate a random ID that can be used to couple up- and downstream URLs */
	private String getPair() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(96, random).toString(32);
	}

	public void shutdown() {
		moreAudioLeft = false;
	}

	/**
	 * write audio data to the stream that is connected to our Google-Upstream
	 * 
	 * @throws IOException
	 * @throws LineUnavailableException
	 */
	private void writeToStream(OutputStream stream, AudioInputStream ai) throws IOException, LineUnavailableException {
		byte tempBuffer[] = new byte[BUFFER_SIZE];
		FLACFileWriter ffw = new FLACFileWriter();
		ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFFER_SIZE);

		moreAudioLeft = true;
		int delay = 0;
		double level = 0;
		while (moreAudioLeft) {
			int cnt = -1;
			cnt = ai.read(tempBuffer, 0, BUFFER_SIZE);
			if (cnt > 0) {// if there is data
				if (vadCalibration) {
					if (timeStart == 0) {
						LocalMessageIU localIU = new LocalMessageIU();
						localIU.setCategory("vad");
						localIU.getPayload().put("calibration", "start");
						outBuffer.add(localIU);
						timeStart = System.currentTimeMillis();
					}
					if (System.currentTimeMillis() - timeStart < 2000) {
						caliSum += calculateRMSLevel(tempBuffer);
						caliCount++;
					} else {
						double caliAvg = caliSum / (double) caliCount;
						volumeThresholdSilence = (int) caliAvg + 10;
						volumeThresholdVoice = (int) caliAvg + 25;
						vadCalibration = false;
						LocalMessageIU localIU = new LocalMessageIU();
						localIU.setCategory("vad");
						localIU.getPayload().put("calibration", "finished");
						localIU.getPayload().put("silence", String.valueOf(volumeThresholdSilence));
						localIU.getPayload().put("speech", String.valueOf(volumeThresholdVoice));
						outBuffer.add(localIU);
					}
				}

				if (vadActive && !vadCalibration) {
					// FLAG: VoiceActivityDetection

					level = calculateRMSLevel(tempBuffer);

					if (isSpeaking) {
						if (level < volumeThresholdSilence) {
							delay++;
						} else {
							delay = 0;
						}
					} else {
						if (level > volumeThresholdVoice) {
							delay++;
						} else {
							delay = 0;
						}
					}

//					LocalMessageIU localIUTemp = new LocalMessageIU();
//					localIUTemp.setCategory("vad");
//					localIUTemp.getPayload().put("level", String.valueOf(level));
//					outBuffer.add(localIUTemp);
					
					
					if (isSpeaking && level < volumeThresholdSilence && delay > delayThreshold) {
						LocalMessageIU localIU = new LocalMessageIU();
						localIU.setCategory("vad");
						localIU.getPayload().put("userSpeaks", "0");
						outBuffer.add(localIU);
						isSpeaking = false;
					} else if (!isSpeaking && level > volumeThresholdVoice && delay > delayThreshold) {

						LocalMessageIU interruptIU = new LocalMessageIU();
						interruptIU.setCategory("vad");
						interruptIU.getPayload().put("interruptRequest", "1");
						outBuffer.add(interruptIU);

						LocalMessageIU localIU = new LocalMessageIU();
						localIU.setCategory("vad");
						localIU.getPayload().put("userSpeaks", "1");
						outBuffer.add(localIU);
						isSpeaking = true;

					}

					notifyListeners();
				}

				InputStream byteInputStream = new ByteArrayInputStream(tempBuffer);
				AudioInputStream ais = new AudioInputStream(byteInputStream, ai.getFormat(), cnt);

				try {
					ffw.write(ais, FLACFileWriter.FLAC, baos);// convert audio
				} catch (Exception e) {
					this.logger.warn("Could not write " + cnt + " bytes of data using FLACFileWriter.");
				}
				stream.write(baos.toByteArray());// write FLAC audio data to
													// the output stream to
													// google
				baos.reset();
			} else {
				if (ai instanceof FrontEndBackedAudioInputStream || (!((FrontEndBackedAudioInputStream) ai).hasMoreAudio())) {
					moreAudioLeft = false;
				}
			}
		}
		stream.write(FINAL_CHUNK);
		stream.close();
	}

	protected static double calculateRMSLevel(byte[] audioData) {
		long lSum = 0;
		for (int i = audioData.length - 1; i > 0; i--) {
			lSum = lSum + audioData[i];
		}

		double dAvg = lSum / audioData.length;

		double sumMeanSquare = 0d;
		for (int j = audioData.length - 1; j > 0; j--) {
			sumMeanSquare = sumMeanSquare + Math.pow(audioData[j] - dAvg, 2d);
		}
		double averageMeanSquare = sumMeanSquare / audioData.length;

		return Math.pow(averageMeanSquare, 0.5d) + 0.5;
	}

	/** connect to Google and receive JSON results */
	class LiveJSONListener extends GoogleJSONListener {
		HttpURLConnection con;
		/** used to terminate this thread */
		boolean inShutdown = false;

		LiveJSONListener(String pair) throws IOException {

			this.con = getDownConnection(pair);
		}

		void shutdown() {
			inShutdown = true;
		}

		@Override
		public void run() {

			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String decodedString;
				while (!inShutdown && (decodedString = in.readLine()) != null) {

					// FLAG: ASR(raw)
					LocalMessageIU localIU = new LocalMessageIU();
					localIU.setCategory("asrraw");
					localIU.getPayload().put("asrresult", decodedString);
					if(!decodedString.equals("{\"result\":[]}"))
						outBuffer.add(localIU);
					
					if(!asrDemo)
						processJSON(decodedString);
				}
				terminateDump();
			} catch (Exception e) {
				// con.disconnect();
				// throw new RuntimeException(e);
			} finally {
				con.disconnect();
			}
		}
	}

	/** consume timed JSON results from a file or URL */
	class PlaybackJSONListener extends GoogleJSONListener {
		BufferedReader input;

		public PlaybackJSONListener(URL jsonDumpInput) throws IOException {
			input = new BufferedReader(new InputStreamReader(jsonDumpInput.openStream()));
		}

		void shutdown() {
		} // ignore this

		@Override
		public void run() {
			try {
				String timedJSON;
				while ((timedJSON = input.readLine()) != null) {
					String[] split = timedJSON.split("\t", 2);
					int time = Integer.parseInt(split[0]);
					String json = split[1];
					long remainingDelay = time - getTotalElapsedTime();
					assert remainingDelay > 0 : remainingDelay;
					if (remainingDelay > 0) {
						Thread.sleep(remainingDelay);
					}
					processJSON(json);
				}
				terminateDump();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * listen for incoming JSON results coming from Google's downstream
	 * connection
	 * 
	 * incoming results are analyzed, turned into IUs, amended with (guessed)
	 * time-stamp information, and finally pushed to our RightBuffer and
	 * listener updated
	 */
	abstract class GoogleJSONListener implements Runnable {
		private long initialTime; // in milliseconds
		private long chunkStartTime; // in milliseconds
		private long prevEndTime; // in milliseconds
		private final IUList<WordIU> chunkHyps; // list of WordIUs for the
												// current "chunk" (Google may
												// split hypotheses into
												// separate chunks)
		private int lastResultIndex;
		private WordIU prev;
		private FileWriter dumpOutput;

		GoogleJSONListener() throws IOException {
			prevEndTime = 0;
			chunkHyps = new IUList<WordIU>();
			setLastResultIndex(-1);
			prev = TextualWordIU.FIRST_ATOMIC_WORD_IU;
			if (jsonDumpOutput != null) {
				dumpOutput = new FileWriter(jsonDumpOutput);
			}
			initialTime = getTimestamp();
			setChunkStartTime(initialTime);
		}

		/* functionality of passing results to a dumpFile */

		void processJSON(String decodedString) throws IOException {
			if (decodedString != null) {
				if (dumpOutput != null) {
					dumpOutput.write(Long.toString(getTotalElapsedTime()));
					dumpOutput.write("\t");
					dumpOutput.write(decodedString);
					dumpOutput.write("\n");
				}
				parseJSON(decodedString);
			}
		}

		synchronized void terminateDump() throws IOException {
			if (dumpOutput != null)
				dumpOutput.close();
			dumpOutput = null;
		}

		/* actual result analysis below */

		/**
		 * google result objects consist of an array of one or more result
		 * "segments". segments can be "final" or not, segments can have a
		 * "stability" estimate (non-final ones usually do) segments contain an
		 * array of "alternative" hypotheses, which each consist of a
		 * "transcript" (the words spoken) and may be followed by a "confidence"
		 * (for final hypotheses) only final hypotheses contain multiple
		 * alternative transcripts of one segment one audio file may result in
		 * several result objects (chunks), which are distinguished by their
		 * "result_index" number
		 */
		private void parseJSON(String decodedString) {
			double stabilityThreshold = "stable".equals(jsonAnalysisMode) ? 0.7 : 0.001;
			JSONObject json = new JSONObject(decodedString);
			JSONArray result = json.getJSONArray("result");
			int resultLength = result.length();

			if (resultLength > 0) {
				int transcriptNum = result.getJSONObject(0).getJSONArray("alternative").length();
				String[] results = new String[transcriptNum];
				String transcript = "";
				boolean isFinal = result.getJSONObject(0).has("final");
				if (isFinal && "sticky".equals(jsonAnalysisMode)) {

					assert resultLength == 1 : "oups, I thought \"final\" results only have a single segment";

					int bestCost = Integer.MAX_VALUE;
					for (int i = 0; i < transcriptNum; i++) {
						String testTranscript = result.getJSONObject(0).getJSONArray("alternative").getJSONObject(i)
								.getString("transcript");
						results[i] = testTranscript;
						int cost = 0;
						for (EditMessage<?> em : chunkHyps.diffByPayload(transcriptToWordList(testTranscript))) {
							if (em.getType().equals(EditType.REVOKE)) {
								cost += 1;
							}
						}
						if (cost < bestCost) {
							bestCost = cost;
							transcript = testTranscript;
						}
					}
				} else {
					String testTranscript = "";
					for (int i = 0; i < resultLength; i++) {

						testTranscript += result.getJSONObject(i).getJSONArray("alternative").getJSONObject(0).getString("transcript");

						JSONObject resultSegment = result.getJSONObject(i);
						double stability = resultSegment.optDouble("stability", 1.0);
						if (stability > stabilityThreshold) {
							transcript += resultSegment.getJSONArray("alternative").getJSONObject(0).getString("transcript");
						}
					}
					results[0] = testTranscript;
				}
				LocalMessageIU localIU = new LocalMessageIU();
				localIU.getPayload().put("asrtest", "GoogleASR-Dup:" + System.currentTimeMillis() + ":" + Arrays.toString(results));
				localIU.setCategory("asrtest");

				outBuffer.add(localIU);

				int resultIndex = json.getInt("result_index");
				if (!"".equals(transcript) && (!robust || isFinal)) {
					updateCurrentHyps(transcript, resultIndex, isFinal);
					setLastResultIndex(resultIndex);
				}
			}
		}

		private List<WordIU> transcriptToWordList(String transcript) {
			List<WordIU> words = new ArrayList<WordIU>();
			for (String token : Arrays.asList(transcript.toLowerCase().trim().split("\\s+"))) {
				words.add(new WordIU(token, null, null));
			}
			return words;
		}

		private void updateCurrentHyps(String transcript, int resultIndex, boolean isFinal) {

			// FLAG: ASR rightBuffer

			List<String> words = Arrays.asList(transcript.toLowerCase().trim().split("\\s+"));
			IUList<WordIU> currentHyps = new IUList<WordIU>();
			// keep working on the frontier
			if (startsNewChunk(resultIndex)) {
				currentHyps.clear();
				chunkHyps.clear();
			}
			double delta = getElapsedChunkTime() / words.size();
			int currentFrame = (int) (getTotalElapsedTime() * TimeUtil.MILLISECOND_TO_FRAME_FACTOR);
			int wordIndex = 1;
			long lastEndTime = 0;
			for (String word : words) {
				long startTime = prevEndTime + (int) (delta * (wordIndex - 1));
				long endTime = prevEndTime + (int) (delta * wordIndex);
				SegmentIU siu = new SegmentIU(new Label(startTime / TimeUtil.SECOND_TO_MILLISECOND_FACTOR, endTime
						/ TimeUtil.SECOND_TO_MILLISECOND_FACTOR, word));
				lastEndTime = endTime;
				WordIU wiu = new WordIU(word, prev, Collections.<IU> singletonList(siu));
				prev = wiu;
				currentHyps.add(wiu);
				wordIndex++;
			}
			// This calculates the differences between the current IU list and
			// the previous, based on payload
			List<EditMessage<WordIU>> diffs = chunkHyps.diffByPayload(currentHyps);
			chunkHyps.clear();
			chunkHyps.addAll(currentHyps);
			if (isFinal) {
				// add remaining WordIUs and commit
				for (WordIU wordIU : chunkHyps) {
					diffs.add(new EditMessage<WordIU>(EditType.COMMIT, wordIU));
				}
				currentHyps.clear();
				chunkHyps.clear();
				prev = TextualWordIU.FIRST_ATOMIC_WORD_IU;
			}
			List<WordIU> ius = new ArrayList<WordIU>();
			for (EditMessage<WordIU> edit : diffs)
				ius.add(edit.getIU());
			if (startsNewChunk(wordIndex)) {
				setChunkStartTime(getTimestamp());
				prevEndTime = lastEndTime;
			}
			// The diffs represents what edits it takes to get from prevList to
			// list, send that to the right buffer
			for (PushBuffer listener : iulisteners) {
				if (listener == null)
					continue;
				if (listener instanceof FrameAware)
					((FrameAware) listener).setCurrentFrame(currentFrame);
				// update frame count in frame-aware pushbuffers
				if (!diffs.isEmpty())
					listener.hypChange(ius, diffs);
			}
			notifyListeners();
		}

		abstract void shutdown();

		long getTimestamp() {
			return System.currentTimeMillis();
		}

		protected long getTotalElapsedTime() {
			return getTimestamp() - initialTime;
		}

		private long getElapsedChunkTime() {
			return getTimestamp() - chunkStartTime;
		}

		private void setChunkStartTime(long startTime) {
			this.chunkStartTime = startTime;
		}

		private int getLastResultIndex() {
			return lastResultIndex;
		}

		private void setLastResultIndex(int lastResultIndex) {
			this.lastResultIndex = lastResultIndex;
		}

		private boolean startsNewChunk(double i) {
			return i > getLastResultIndex();
		}

	}

	public void setImportFile(URL url) {
		jsonDumpInput = url;
	}

	public void setExportFile(File file) {
		jsonDumpOutput = file;
	}

	public void setAPIKey(String apiKey) {
		googleAPIkey = apiKey;
	}

	public void setSamplingRate(String SamplingRate) {
		samplingRate = SamplingRate;
	}

	public void setLanguageCode(String string) {
		languageCode = string;
	}

}
