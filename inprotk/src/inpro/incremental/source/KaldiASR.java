package inpro.incremental.source;


import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.LinkedList;
import java.util.List;

import kaldi.Kaldi;
import kaldi.OutputProcess;
import inpro.annotation.Label;
import inpro.audio.FrontEndBackedAudioInputStream;
import inpro.incremental.FrameAware;
import inpro.incremental.PushBuffer;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.IUList;
import inpro.incremental.unit.SegmentIU;
import inpro.incremental.unit.WordIU;
import inpro.util.TimeUtil;
import edu.cmu.sphinx.frontend.BaseDataProcessor;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Integer;
import edu.cmu.sphinx.util.props.S4String;

public class KaldiASR extends IUSourceModule {
	
	
	@S4Integer(defaultValue = 5010)
	public final static String PROP_PORT = "port";
	private Integer port;
	
	@S4String(defaultValue = "localhost")
	public final static String PROP_SERVER = "server";
	
	private String server;
	
	private final FrontEndBackedAudioInputStream ais;
	
	private Kaldi asr = null;
	private KaldiOutputProcess output;
	private IUList<WordIU> chunkHyps;
	private IUList<WordIU> currentHyps;
	private WordIU prev;
	public SegmentIU prevSiu;
	
	
	private TreeMap<Float,TreeMap<Float,String>> workingIUs;
	private List<EditMessage<WordIU>> diffs;
	
	/** enable Sphinx configuration */
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		this.setServer(ps.getString(PROP_SERVER));
		this.setPort(ps.getInt(PROP_PORT));
		diffs = new ArrayList<EditMessage<WordIU>>();
	}
	
	public void setPort(int port) {
		this.port = port;
	}

	public void setServer(String server) {
		this.server = server;
	}
	
	protected long getTotalElapsedTime() {
		return getTimestamp() - ais.getAudioStartTime(); //initialTime;
	}
	
	long getTimestamp() {
		return  System.currentTimeMillis();
	}

	public KaldiASR(BaseDataProcessor frontend) {
	
		ais = new FrontEndBackedAudioInputStream(frontend);
		output = new KaldiOutputProcess();
		this.chunkHyps = new IUList<WordIU>();
		this.currentHyps = new IUList<WordIU>();
		workingIUs = new TreeMap<Float,TreeMap<Float,String>>();
		prev = WordIU.FIRST_WORD_IU;
	}
	
	public KaldiASR() {
		ais = null;
		output = new KaldiOutputProcess();
		this.chunkHyps = new IUList<WordIU>();
		this.currentHyps = new IUList<WordIU>();
		workingIUs = new TreeMap<Float,TreeMap<Float,String>>();
		prev = WordIU.FIRST_WORD_IU;
	}
	
	private void init() {
		try {
			
			if (asr == null)
				asr = new Kaldi(server, port);
			
		} 
		catch (UnknownHostException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		if (asr != null) {
			try {
				asr.close();
			} 
			catch (IOException e1) {
				e1.printStackTrace();
			}
			asr = null;
		}
	}
	
	public void recognize() {
		
		init();
		
		try {
			asr.recognize(ais, output);
		} 
		catch (IOException e) {
			throw new RuntimeException("Kaldi Server connection broken!");
		} 
		finally {
			close();
		}
	}
	
	private class KaldiOutputProcess extends OutputProcess {
		
		@Override
		public void endOfUtt() {
//			 commit();
		}
		
		private void commit() {
			 synchronized(chunkHyps) {
				for (WordIU wordIU : chunkHyps) {
					diffs.add(new EditMessage<WordIU>(EditType.COMMIT, wordIU));
				}
				workingIUs.clear();
				currentHyps.clear();
				chunkHyps.clear();
				prev = WordIU.FIRST_WORD_IU;
				notifyBuffers();
				diffs.clear();
			}
		}
		
		@Override
		public void newSet() {
//			currentHyps.clear();
		}

		@Override
		public void addWord(Word word) {
			add(word);
			
			currentHyps = getIUs();
//			System.out.println("cur: " + currentHyps);
//			System.out.println("chu: " + chunkHyps);
			diffs = chunkHyps.diffByPayload(currentHyps);
//			System.out.println("dif: " + diffs);
			chunkHyps.clear();
			chunkHyps.addAll(currentHyps);
			notifyBuffers();
		}

		private void notifyBuffers() {
			int currentFrame = (int) (getTotalElapsedTime() * TimeUtil.MILLISECOND_TO_FRAME_FACTOR); 
			LinkedList<WordIU> ius = new LinkedList<WordIU>();
			for (EditMessage<WordIU> edit: diffs) { 
				ius.add(edit.getIU()); 
			}
			for (PushBuffer listener : iulisteners) {
				if (listener == null) continue;
				if (listener instanceof FrameAware)
					((FrameAware) listener).setCurrentFrame(currentFrame);
				// update frame count in frame-aware pushbuffers
				if (!diffs.isEmpty())
					listener.hypChange(ius, diffs);
			}
			notifyListeners();	
		}
		
		private IUList<WordIU> getIUs() {
			IUList<WordIU> newIUs = new IUList<WordIU>();
			
			for (Float start : workingIUs.keySet()) {
				for (Float end : workingIUs.get(start).keySet()) {
					String word = workingIUs.get(start).get(end);
					SegmentIU siu = new SegmentIU(new Label(start, end, word)); 
					List<IU> gIns = new LinkedList<IU>();
					gIns.add(siu);
					WordIU wiu = new WordIU(word, prev, gIns);
					newIUs.add(wiu);
				}
			}
			return newIUs;
		}

		private void add(Word word) {
			
			Float start = word.start;
			Float end = word.end;
			String w = word.word.toLowerCase();
			
//			Note: there is the case where an old word is introduced again. Do we ignore that? 
//			At the moment we are revoking and adding new, as there is generally something new after it.
			
//			ignore duplicates
			if (workingIUs.containsKey(start))
				if (workingIUs.get(start).containsKey(end))
						if (workingIUs.get(start).get(end).equals(w))
							return;
			
//			Cleanup: remove any words that end before the new word starts
//			System.out.println(workingIUs);
//			System.out.println(word.word  + "  "+ word.start + " " +word.end);
			ArrayList<Float> toRemove = new ArrayList<Float>();
			for (Float s : workingIUs.keySet()) { 
//				System.out.println(s  + " " + start + " " + workingIUs.get(s));
				if (start < s) toRemove.add(s);
			}
//			System.out.println(toRemove);
			for (Float s: toRemove) workingIUs.remove(s);
			
			// this will always overwrite words with the same start time
			workingIUs.put(start, new TreeMap<Float,String>());
			
			// this will always overwrite words with the same end time
			workingIUs.get(start).put(end, w);
		}
	}
}
