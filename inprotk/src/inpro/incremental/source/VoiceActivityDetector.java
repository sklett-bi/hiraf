package inpro.incremental.source;

import java.util.ArrayList;
import java.util.List;

import edu.cmu.sphinx.frontend.FrontEnd;
import edu.cmu.sphinx.frontend.endpoint.SpeechEndSignal;
import edu.cmu.sphinx.frontend.endpoint.SpeechStartSignal;
import edu.cmu.sphinx.util.props.Configurable;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4ComponentList;
import inpro.incremental.PushBuffer;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.VoiceActivityIU;
import inpro.sphinx.frontend.QuickSpeechMarker;
import inpro.util.TimeUtil;
import edu.cmu.sphinx.frontend.BaseDataProcessor;
import edu.cmu.sphinx.frontend.Data;
import edu.cmu.sphinx.frontend.DataEndSignal;
import edu.cmu.sphinx.frontend.DataProcessingException;

public class VoiceActivityDetector extends BaseDataProcessor implements Configurable {
	
	/*
	 * IUModule for Voice Activity Detection (VAD). Actually, the VAD is done by the speechClassifier and then cleaned up by the speechMarker
	 * so we take advantage of that here by noting changes in state and passing that information along as an IU. 
	 * 
	 * Note that this class is both a BaseDataProcesor (i.e., it is part of the Sphinx frontend and should come directly after the speechMarker 
	 * of class QuickSpeechMarker) and an IUModule of sorts (though, like SphinxASR, the iulisteners are invoked by hand here)
	 */

	@S4Component(type = FrontEnd.class, mandatory = true)
	public final static String PROP_ASR_FRONTEND = "frontend";
	
	@S4Component(type = QuickSpeechMarker.class, mandatory = true)
	public final static String PROP_SPEECH_MARKER = "speechMarker"; 
	
	@S4ComponentList(type = PushBuffer.class)
	public final static String PROP_HYP_CHANGE_LISTENERS = "hypChangeListeners";
	public final List<PushBuffer> iulisteners = new ArrayList<PushBuffer>();
	
	public enum State { IN_SPEECH, NON_SPEECH, DATA_END } //copying this unapologetically from the QuickSpeechMarker class
	
	static List<EditMessage<VoiceActivityIU>> edits = new ArrayList<EditMessage<VoiceActivityIU>>();
	private State state;
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		
		super.newProperties(ps);
		iulisteners.clear();
		iulisteners.addAll(ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class));
	}

	@Override
	public Data getData() throws DataProcessingException {
		
		Data d = readData();
		if (d instanceof SpeechStartSignal) {
			SpeechStartSignal sd = (SpeechStartSignal) d;
			handlePotentialStateChange(state == State.IN_SPEECH, true, sd.getTime());
			state = State.IN_SPEECH;
		}
		else if (d instanceof SpeechEndSignal) {
			SpeechEndSignal sd = (SpeechEndSignal) d;
			handlePotentialStateChange(state == State.IN_SPEECH, false, sd.getTime());
			state = State.NON_SPEECH;
		}
		 else if (d instanceof DataEndSignal) {
			state = State.DATA_END;
		}
		
		return d; // don't do anything to the data, just read it and pass it along
	}
	
	private void handlePotentialStateChange(boolean inSpeech, boolean isSpeech, double collectTime) {
		if (inSpeech) {
			if (!isSpeech) {
				System.out.println("////////////////switched from S to NS");
//				we have a change of state from being in speech to now moving out of speech, so signal that
				VoiceActivityIU vaIU = new VoiceActivityIU(false, collectTime/TimeUtil.SECOND_TO_MILLISECOND_FACTOR);
				edits.add(new EditMessage<VoiceActivityIU>(EditType.ADD, vaIU));
			}
		}
		if (!inSpeech) {
			if (isSpeech) {
				System.out.println("///////////////switched from NS to S");
//				we now have a change of state from being out of speech to in speech, so signal that
				VoiceActivityIU vaIU = new VoiceActivityIU(true, collectTime/TimeUtil.SECOND_TO_MILLISECOND_FACTOR);
				edits.add(new EditMessage<VoiceActivityIU>(EditType.ADD, vaIU));
			}
		}
		notifyListeners(iulisteners);
	}

	private Data readData() throws DataProcessingException {
        return getPredecessor().getData();
    }
	
	static public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			//logger.debug("notifying about" + edits);
			for (PushBuffer listener : listeners) {
				listener.hypChange(null, edits);
			}
			edits = new ArrayList<EditMessage<VoiceActivityIU>>();
		}
	}
	
	public void addListener(PushBuffer pb) {
		iulisteners.add(pb);
	}
	
	
}
