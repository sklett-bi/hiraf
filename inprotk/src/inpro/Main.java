package inpro;

import inpro.apps.SimpleReco;
import inpro.apps.SimpleText;
import inpro.apps.util.RecoCommandLineParser;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.TextBasedFloorTracker;
import inpro.incremental.source.GoogleASR;
import inpro.incremental.source.IUDocument;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import ipaaca.Initializer;
import ipaaca.OutputBuffer;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import rsb.Factory;
import rsb.converter.DefaultConverterRepository;
import rsb.converter.ProtocolBufferConverter;
import rst.dialog.SpeechHypothesesType.SpeechHypotheses;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;

public class Main {
	
	private static boolean asrActive = true;
	
	static Logger log = Logger.getLogger("root");
	public static OutputBuffer outBuffer = new OutputBuffer("main");

	public static boolean openBrowser = false;


	@S4Component(type = TextBasedFloorTracker.class)
	public final static String PROP_FLOOR_MANAGER = "textBasedFloorTracker";

	GoogleASR webSpeech;
	private static PropertySheet ps;

	private static IUDocument iuDocument;

	private static String googleApiKey = "";
	// private List<PushBuffer> hypListeners;
	List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();

	private void run() throws InterruptedException, PropertyException, IOException,
			UnsupportedAudioFileException {

		log.setLevel(Level.OFF);
		
		Initializer.initializeIpaacaRsb();
		
		RSBConfig.initConfig();


		final ProtocolBufferConverter<SpeechHypotheses> converter = new ProtocolBufferConverter<SpeechHypotheses>(
				SpeechHypotheses.getDefaultInstance());

		DefaultConverterRepository.getDefaultConverterRepository().addConverter(converter);

		final Factory factory = Factory.getInstance();
		try {

			RSBConfig.informer = factory.createInformer(RSBConfig.scope);
			RSBConfig.informer.activate();
						
//			informer.deactivate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ConfigurationManager cm = new ConfigurationManager(new File(
				"src/main/java/config/config.xml").toURI().toURL());	
		
	
		
		if(googleApiKey.equals("")) {
			googleApiKey = cm.getPropertySheet("googleASR").getString("apiKey");
		}
		
		
		if(asrActive) {
			System.out.println("using google speech api key: " + googleApiKey);
			System.out.println("starting google asr...");
		}
		
		cm.lookup("vad");		
		

		// for Google ASR
		webSpeech = (GoogleASR) cm.lookup("googleASR");
		RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] { "-M", "-G",
				googleApiKey });
		if(asrActive)
			startGoogleASR(cm, rclp);
		

		TextBasedFloorTracker textBasedFloorTracker = (TextBasedFloorTracker) cm
				.lookup(PROP_FLOOR_MANAGER);
		iuDocument = new IUDocument();
		iuDocument.setListeners(webSpeech.iulisteners);
		SimpleText.createAndShowGUI(webSpeech.iulisteners, textBasedFloorTracker);


		
	}

	private void startGoogleASR(ConfigurationManager cm, RecoCommandLineParser rclp) {
		new Thread() {
			public void run() {

				try {
					SimpleReco simpleReco = new SimpleReco(cm, rclp);
					while (true) {
						try {

							new Thread() {
								public void run() {
									try {
										simpleReco.recognizeOnce();
									} catch (PropertyException e) {
										e.printStackTrace();
									}

								}
							}.start();

							Thread.sleep(10000);
							webSpeech.shutdown();
							// simpleReco.shutdownMic();
						}

						catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

				} catch (PropertyException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (UnsupportedAudioFileException e1) {
					e1.printStackTrace();
				}
			}
		}.start();

	}

	public static void main(String[] args) {
		
		
		try {
			new Main().run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (PropertyException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		}
	}

	public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			// logger.debug("notifying about" + edits);
			for (PushBuffer listener : listeners) {
				listener.hypChange(null, edits);
			}
			edits = new ArrayList<EditMessage<IU>>();
		}
	}

}
