package inpro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import rsb.Informer;

public class RSBConfig {
	

	static String scope = "/example/informer";

	public static Informer<Object> informer;
	
	public static void main(String... args) {

		RSBConfig.initConfig();

	}

	public static void initConfig() {

		checkRSB();
		setScope();

	}

	public static void setScope() {

		String parsedScope;
		File f = new File("asr.conf");
		FileInputStream fis;
		try {
			fis = new FileInputStream(f);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			parsedScope = br.readLine();
			br.close();
			System.out.println(parsedScope + " from " + f.getAbsolutePath());
			scope = parsedScope;
		} catch (FileNotFoundException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is missing");
		} catch (IOException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is corrupt");
		}

	}

	public static void checkRSB() {
		
		File f = new File("rsb.conf");
		@SuppressWarnings("unused")
		FileInputStream fis;
		try {
			fis = new FileInputStream(f);
			System.out.println("rsb.conf found on " + f.getAbsolutePath());
		} catch (FileNotFoundException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is missing");
		}

	}

}
