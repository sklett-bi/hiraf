# Description #

This repository bundles several other projects into one dialogsystem framework. After following the setup, you should be able to create your own dialogsystem using a SpeechRecognizer as input and a speaking 3D virtual avatar as output.

# Setup #

### Instructions for Image-Setup: ###

Create a backup of your machine. If you mistype something in this installation process, you might lose all your files and programs.

Download this image (~15 GB):

https://www.techfak.uni-bielefeld.de/ags/soa/download/esslli-hiraf-usb.iso

Boot from your Linux-Distribution and plug in a USB 3.0 stick with at least 16 GB of space.  
Open a terminal.  
Run 'sudo fdisk -l' and identify the plugged in USB stick.  

    Disk /dev/sdb: 14,4 GiB, 15493758976 bytes, 30261248 sectors

In this example, the USB stick's location is /dev/sdb because it is the only disk matching the size of the actual USB stick.  
Run 'sudo -i' and enter your admin password.  
Run 'dd if=/path/to/the/downloaded/image of=/usbstick/location/  
In the mentioned example, you would need to replace /usbstick/location/ with '/dev/sdb'  
Be very careful, dd can ruin your whole hard drive if you make a mistake like typing /dev/sda.  
dd is by default silent, so you need to wait until it returns (may take a long time, >30 minutes).


After dd returned (signalled by an unblocked prompt in the terminal), you can reboot your machine. At the beginning of the boot process, choose to boot from USB. If you're not sure which boot option is the right one, boot to the bootmenu with the usb stick unplugged and look out which option is added when you boot with the usb stick plugged in.


When it has finished booting, connect the machine to an internet connection (needed for the ASR) and read the manual located on the desktop.

# Contact #

If you have any questions regarding this project, send an email to: sklett@techfak.uni-bielefeld.de
