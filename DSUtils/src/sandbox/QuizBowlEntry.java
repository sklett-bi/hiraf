package sandbox;
import java.util.List;


public class QuizBowlEntry {

	public int id;
	public String answer;
	public String category;
	public List<String> questionChunks;
	
	@Override
	public String toString() {
		String result = "";
		result += answer + "\n";
		result += category + "\n";
		result += questionChunks;
		
		return result;
	}
	
}
