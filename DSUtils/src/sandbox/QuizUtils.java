package sandbox;
import java.util.List;
import java.util.Random;


public class QuizUtils {

	List<QuizBowlEntry> quizEntries;
	QuizBowlEntry activeEntry;
	int chunkIndex = 0;
	
	public QuizUtils() {
		quizEntries = QuizBowlParser.createEntries();
	}
	
	public void startQuestion() {
		
		chunkIndex = 0;
		Random r = new Random();
		int index = r.nextInt(quizEntries.size());
		activeEntry = quizEntries.get(index);
		System.out.println(activeEntry.questionChunks.get(chunkIndex));
		try {
			BMLAction.sendSocketMessage(activeEntry.questionChunks.get(chunkIndex));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void offerAnswer(String question) {
		
		String answer = quizEntries.get(Integer.parseInt(question.replace("question", "")) - 1).answer;
		System.out.println("is " + answer + " correct?");
		try {
			BMLAction.sendSocketMessage("is " + answer + " correct?");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void continueQuestion() {
		
		if(chunkIndex < activeEntry.questionChunks.size() - 1)
			chunkIndex++;
		
//		System.out.println(activeEntry.questionChunks.get(chunkIndex));
		try {
			BMLAction.sendSocketMessage(activeEntry.questionChunks.get(chunkIndex));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void checkAnswer(String answer) {
		String utterance = "";
		if(activeEntry.id == Integer.parseInt(answer.replace("answer", ""))) {
			System.out.println("nice job!");
			utterance = "nice job!";
		}else{
			System.out.println("no, try again.");
			utterance = "no, try again";
		}
		try {
			BMLAction.sendSocketMessage(utterance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
