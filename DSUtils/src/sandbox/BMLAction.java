package sandbox;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class BMLAction {

	String utterance;
	String prepare;
	String activate;

	public String ipAsap = "";

	public static void main(String... args) throws Exception {
		BMLAction asd = new BMLAction();

//		asd.adjustBMLByProfile(null, null);
//		
//		if(true)
//			return;
		
		
		String j = "<speak version=\"1.0\"" + "xmlns=\"http://www.w3.org/2001/10/synthesis"
				+ "xml:lang=\"de-DE\">" +

				"Hallo" + "</speak>";

		String gazeToLeftMonitorWhileSpeaking = "<bml xmlns=\"http://www.bml-initiative.org/bml/bml-1.0\"  id=\"bml1\">"
				+ "<speech id=\"speech1\"><text>Hallo Welt.</text></speech>\n"
//				+ "<gazeShift id=\"gaze1\" influence=\"WAIST\"  target=\"1,0.8,1\" />"
//				+ "<gazeShift id=\"gaze2\" start=\"2\" end=\"5\" influence=\"WAIST\"  target=\"0,0.5,1\" />"
				+ "</bml>";

		String defaultGazeWhileSpeaking = "<bml xmlns=\"http://www.bml-initiative.org/bml/bml-1.0\"  id=\"bml1\" xmlns:bmlt=\"http://hmi.ewi.utwente.nl/bml\">"
				+ "<speech id=\"s1\">"
//				+ "<text>Hallo Welt.</text>"
				+ "<description priority=\"1\" type=\"application/ssml+xml\">"
				+ "<speak xmlns=\"http://www.w3.org/2001/10/synthesis\">"
				+ "<prosody volume=\"+90.0dB\">Hallo Welt2</prosody></speak>"
				+ "</description>"
				+ "</speech>\n"
//				+ "<gaze id=\"gaze1\" start=\"0\" end=\"10000\"  influence=\"SHOULDER\"  target=\"4,0.6,5\" />"
//				+ "<gaze id=\"gaze2\" start=\"0\" end=\"10000\" influence=\"SHOULDER\"  target=\"0,0.6,5\" />"
				+ "</bml>";                                                    
		
		String interrupt = "<bml xmlns=\"http://www.bml-initiative.org/bml/bml-1.0\"  id=\"bml1\"><bmla:interrupt xmlns:bmla=\"http://www.asap-project.org/bmla\" id=\"i1\" target=\"bml1\" start=\"0\" /></bml>";
		
//		-32 r1, 32 l1,, rshoulder=0,6,6
		String head = ""  //"<bml xmlns=\"http://www.bml-initiative.org/bml/bml-1.0\"  id=\"bml1\">"
				+ "<speech id=\"s1\"><text>";
		String tail = "</text></speech></bml>";
		String phonemText = "<phoneme alphabet=\"ipa\" " +
				"ph=\"&#x2C8;l&#x251; &#x2C8;vi&#x2D0;&#x27E;&#x259; &#x2C8;&#x294;e&#x26A; &#x2C8;b&#x25B;l&#x259;\">" +
				"La vita è bella </phoneme>";
		
		asd.sendSocketMessage(defaultGazeWhileSpeaking);

		// Thread.sleep(1500);

		// asd.sendSocketMessage("tts.cancel");
	}

	

	public static void sendSocketMessage(String message) throws Exception {

		String ip = "129.70.139.101";
		int port = 3147;
		Socket socket = new Socket(ip, port);

		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

		printWriter.write(message);

		printWriter.flush();

		socket.close();

	}
	
	

	public String getUtterance() {
		return utterance;
	}

	public void setUtterance(String utterance) {
		this.utterance = utterance;
	}

	public String getPrepare() {
		return prepare;
	}

	public void setPrepare(String prepare) {
		this.prepare = prepare;
	}

	public String getActivate() {
		return activate;
	}

	public void setActivate(String activate) {
		this.activate = activate;
	}

}
