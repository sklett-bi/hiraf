package sandbox;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuizBowlParser {

	public static void main(String[] args) {
		List<QuizBowlEntry> questions = new ArrayList<QuizBowlEntry>();
		File f = new File("questions.csv");
		FileInputStream fis;
		try {
			fis = new FileInputStream(f);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line = "";
			int id = 0;
			while (line != null) {
				
				line = br.readLine();
				
				if(line != null && line.contains("\"")) {
				line = line.replace("\"", "");
				
				}
				if (id > 10 || line == null || !line.contains(","))
					continue;

				line = line.toLowerCase();
				String[] split = line.split(",", 5);
				QuizBowlEntry entry = new QuizBowlEntry();
				System.out.println(id);
				entry.id = id;
				id++;
				entry.answer = split[2];
				entry.category = split[3];
				
				String[] questionChunks = split[4].split("\\|\\|\\|");
				List<String> listChunks = new ArrayList<String>();
				for (String chunk : questionChunks) {	
					listChunks.add(chunk);
				}
				entry.questionChunks = listChunks;
				questions.add(entry);
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is missing");
		} catch (IOException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is corrupt");
		}
		
		createQuestions(questions);
		
	}
	
	public static List<QuizBowlEntry> createEntries() {
		List<QuizBowlEntry> questions = new ArrayList<QuizBowlEntry>();
		File f = new File("questions.csv");
		FileInputStream fis;
		try {
			fis = new FileInputStream(f);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line = "";
			int id = 0;
			while (line != null) {

				line = br.readLine();
				if(line != null && line.contains("\""))
				line = line.replace("\"", "");
				
				if (id > 10 || line == null || !line.contains(","))
					continue;

				String[] split = line.split(",", 5);
				QuizBowlEntry entry = new QuizBowlEntry();
				entry.id = id;
				id++;
				
				if(id == 1)
					continue;
				
				entry.answer = split[2].toLowerCase();
				entry.category = split[3].toLowerCase();
				String[] questionChunks = split[4].toLowerCase().split("\\|\\|\\|");
				List<String> listChunks = new ArrayList<String>();
				for (String chunk : questionChunks) {
					listChunks.add(chunk);
				}
				entry.questionChunks = listChunks;
				questions.add(entry);
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is missing");
		} catch (IOException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is corrupt");
		}
		
		return questions;
	}

	public static String createAnswers(List<QuizBowlEntry> questions) {
		String json = "{ \"intent\": \"answer\", \n";
		json += "\t\"concepts\": [\n";
		for (int qi = 1; qi < questions.size(); qi++) {

			QuizBowlEntry q = questions.get(qi);
			json += "\t{ \"concept\":\"answer" + q.id + "\",\n";
			json += "\t\t\"properties\": [\n";
			String[] answerProperties = q.answer.split(" ");
			for (int i = 0; i < answerProperties.length; i++) {
				String answerProperty = answerProperties[i];
				if (i != answerProperties.length - 1)
					json += "\t\t{\"property\":\"" + answerProperty + "\"},\n";
				else
					json += "\t\t{\"property\":\"" + answerProperty + "\"}\n";
			}

			if (qi < questions.size() - 1)
				json += "\t\t]\n\n\t},\n";
			else
				json += "\t\t]\n\t}\n";
		}
		json += "\t]\n}";
		System.out.println("json: " + json);
		return json;
	}

	public static String createQuestions(List<QuizBowlEntry> questions) {
		String json = "{ \"intent\": \"question\", \n";
		json += "\t\"concepts\": [\n";
		for (int qi = 1; qi < questions.size(); qi++) {

			QuizBowlEntry q = questions.get(qi);
			json += "\t{ \"concept\":\"question" + q.id + "\",\n";
			json += "\t\t\"properties\": [\n";
			List<String> questionChunks = new ArrayList<String>();
			
			for(String c : q.questionChunks) {
				questionChunks.addAll(Arrays.asList(c.split(" ")));
			}
			for (int i = 0; i < questionChunks.size(); i++) {
				String answerProperty = questionChunks.get(i);
				if (i != questionChunks.size() - 1)
					json += "\t\t{\"property\":\"" + answerProperty + "\"},\n";
				else
					json += "\t\t{\"property\":\"" + answerProperty + "\"}\n";
			}

			if (qi < questions.size() - 1)
				json += "\t\t]\n\n\t},\n";
			else
				json += "\t\t]\n\t}\n";
		}
		json += "\t]\n}";
		System.out.println(json);
		return json;
	}
	
	public static String readString(String filename) {

		String result = "";

		File f = new File(filename);
		FileInputStream fis;
		try {
			fis = new FileInputStream(f);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			result = br.readLine();
			br.close();
		} catch (FileNotFoundException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is missing");
		} catch (IOException e) {
			System.err.println("File '" + f.getAbsolutePath() + "' is corrupt");
		}

		return result;
	}

}
